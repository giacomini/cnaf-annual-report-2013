\documentclass[twoside]{book}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian, english]{babel}
\usepackage[a4paper, width=455.24408pt, height=654.41338pt, top=1.58in, centering]{geometry} % match the jpconf style
\usepackage{pdfpages}
\usepackage{lipsum}
\usepackage{emptypage} % so that empty pages don't have headings and footers
\usepackage{fancyhdr}
\usepackage{tocloft}
\usepackage{titlesec}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage[hidelinks]{hyperref} % this package should come last; toc entries are hypertext links

\renewcommand{\cftpartfont}{\hfil\Large\bfseries}
\renewcommand{\cftpartafterpnum}{\hfil}
\cftpagenumbersoff{part}

\renewcommand{\cftchapleader}{\cftdotfill{\cftdotsep}}

\begin{document}

\title{INFN-CNAF\\
Annual Report 2013}
\date{}
\author{}
%\maketitle

\includepdf[pages=1, pagecommand={\thispagestyle{empty}}]{papers/cover.pdf}

\newpage
\thispagestyle{empty}
~
\vfill

\subsubsection*{INFN-CNAF Annual Report 2013}

\textit{www.cnaf.infn.it/annual-report}\\
ISSN 2283-5490 (online)

\subsubsection*{Editors}

Luca dell'Agnello\\
Francesco Giacomini\\
Claudio Grandi

\subsubsection*{Address}

INFN CNAF\\
Viale Berti Pichat, 6/2\\
I-40127 Bologna\\
Tel. +39 051 209 5763, Fax +39 051 209 5478\\
\textit{www.cnaf.infn.it}

\subsubsection*{Cover Images}

The tape library hosted at the CNAF Tier1 Computing Center.\\
Grid activity as shown by the WLCG dashboard
(\textit{dashboard.cern.ch}). \copyright 2009 GeoBasis-DE/BKG, Image Landsat
\copyright 2014 Google, US Dept. of State Geographer.\\
View of the entrance to the Physics and Astronomy Dept. of the
University of Bologna, where CNAF is hosted.

~

\noindent\textit{Cover design:} Francesca Cuicchio

\cleardoublepage % force a right-side page

\frontmatter
\pagestyle{fancy}
\fancyhf{} % clear headers and footers
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\fancyhf[HLE,HRO]{\thepage}
\fancyhf[HLO,HRE]{\leftmark}

\tableofcontents

\cleardoublepage % force a right-side page

\mainmatter

\phantomsection
\addcontentsline{toc}{chapter}{Introduction}
\markboth{\MakeUppercase{Introduction}}{\MakeUppercase{Introduction}}
\chapter*{Introduction}
\thispagestyle{plain}
%\lipsum

This first CNAF Annual Report is dedicated to CNAF itself: to its
first 50 years, to those who founded it, to those who contributed to
improving it until it became one of the main data centers for
experimental physics and particle physics especially, to those who
succeeded in attracting copious external funding to our
Institute. European funds, regional funds and prize funds have in fact
allowed us to inject our center with innovation and development, to
remain technologically advanced and to face with adequate tools the
increasingly engaging challenges our experiments require. But this
first activity report is primarily dedicated to all CNAF personnel,
both staff and temporary collaborators, who day by day make it happen.

Since 2005, CNAF has hosted the Italian Tier1 computing center for LHC
experiments, but many are the INFN experiments dealing with particle
and astro-particle physics that it has attracted. An approximate 75\%
of the Tier1 computing resources is used by LHC; the rest by other
experiments. The first section of this Annual Report, ``Scientific
Exploitation of CNAF ICT Resources'', gathers and recounts the
experience and results of all users of our Institute. In 2013 new
experiments were added to the list CNAF leads, thus demonstrating
CNAF's importance in Scientific Computing, both nationally and
internationally.
 
The second section, ``The INFN-Tier1 Center and National ICT
Services'', describes the ``data center'', with its architecture and
organization. In 2013 the computing power of the Tier1 reached
200000~HS06 (18000 cores), a fast storage capacity of 13~PB and a
long-term storage capacity (tape) of 15~PB. The computing center also
hosts services of national utility for INFN, such as the top level
domain DNS and high availability core DNS, mail relay services,
mailing lists services, centralized web sites management, NTP.

``Software Services and Distributed Systems'' is the third section of
this report. The highlight is on software development activities,
geographically distributed systems (Cloud and Grid) and related
services (e.g. SRM/StoRM), as well as technological tracking
activities aimed at exploring new technologies in computing systems
(CPU architecture, storage, memory, etc.). Among these activities: the
recent collaboration with the KM3NeT experiment for the development of
their trigger and data acquisition; the completion of the COKA project
on new multicore processors; important activities of technological
transfer of Cloud Computing towards the Public Administration of
Regione Marche.

Year 2013 also set the end of the last European projects financed by
the 7th Framework Programme (e.g. EGI-Inspire) and the opening of
Horizon 2020 calls for proposals. One important activity that was
carried out during this year is in fact the study of the H2020
programme, focused on e-Infrastructures and ICT themes, in order to
identify the most suitable calls for our work programmes and future
development. The project Open City Platform, consisting of 20 partners
led by INFN, with an overall budget of 12 million Euros, was one of
the winners of the call for proposals promoted by MIUR on ``Idee
progettuali per Smart Cities and Communities and Social
Innovation''. CNAF, with an approved budget of half a million Euros,
has been playing a leading role in this project both in managing and
technical procedures. Starting year of all activities: 2014.

Our core activities --- the pillars that guarantee the center's
sustainability and future --- are scientific computing, software
development, management of external projects and technology transfer
towards civil or industrial subjects throughout the Country. They are
complementary activities, but the challenge we face is to make them
grow harmoniously and interdependently, effectively yielding existing
competences and impressive synergies in the diverse areas.

In 2013, CNAF celebrated its first 50 years (1962--2012). The most
important moments of the center have been recounted in the volume
``CNAF 50 Years of Technological Excellence'', which was published on
the occasion of the celebrations. We open this Annual Report with the
introduction to Mauro Morandin's book. Director of CNAF until
September 2013, he saw in digital revolution the common thread uniting
the first glorious ante-litteram ``scanners'' built in our center's
very first quarters, and the refined powerful computing center Tier1
that CNAF hosts nowadays and with which INFN participates in the
analysis of all data resulting from LHC experiments. The most
significant articles in the book can be found in the appendix of this
Annual Report.

\vspace{1cm}

\begin{flushright}
  \parbox{0.7\textwidth}{
    \begin{center}
      \textit{Gaetano Maron}
      \\\textit{CNAF Director}
    \end{center}
  }
\end{flushright}

\cleardoublepage % force a right-side page

\phantomsection
\addcontentsline{toc}{chapter}{CNAF 1962--2012: 50 years of innovation and technological excellence}
\markboth{}{}
\chapter*{CNAF 1962--2012\\
  \LARGE{50 years of innovation and technological excellence}
}
\thispagestyle{plain}

When the National Institute of Nuclear Physics (INFN) created the CNAF
Center in 1962, nobody could have imagined the crucial impact that
digital technologies would play many years later, and their role in
all aspects of scientific activities and, to an increasing extent, of
everybody's life. At that time computers were really big and
intimidating machines that could be talked to only via very
specialized and clumsy interfaces. Digital network did not exist, and
calculations were mostly performed with the help of logarithmic tables
and slide rules.

However, it was at about that time that the very early phase of the
digital revolution began. Scientists in the most advanced fields of
research felt the need to have at their disposal electronics devices
capable of transforming analog information into digital data streams
to be immediately digested by computers, and that urgency paved the
way for a radical technology shift meant to revolutionize the way we
live. The creation of CNAF was an example of a general transition
driven by compelling scientific motivations: far before the
invention of digital photography, physicists had the urgency to
digitize huge amounts of pictures of elementary particle trajectories
that started to be produced at a high rate.

In hindsight, the decision taken by INFN to create CNAF was remarkable
for a number of reasons. First of all, the commitment to build a
Center specifically focused on technological activities showed how
important it was to involve INFN personnel in the conception,
development and operation of the advanced tools required to preserve
competitiveness in cutting edge research. In the following decades,
this approach proved to be vital for INFN scientists, who were able to
play a key role in the most advanced research activities all over the
world.

Secondly, the choice of investing specifically on digital technologies
turned out to be quite far-reaching. Although adapting to the rapidly
evolving technological context, the original mission of CNAF to be
at the forefront of digital technologies has basically remained the
same for more than 50 years, and it is still the core of its
activities today. Finally, the special location of CNAF (which is
firmly embedded in the University context) was part of a general
strategic plan that saw INFN pursuing to keep their activities
strongly linked to that fertile and inspirational environment.  The
story of CNAF is the story of how Science can stimulate and exploit
novel technological developments for its own benefits, and possibly
reshape the way our society works. This book is a recollection of the
exceptional story of this project, and a tribute to the ingenuity and
commitment of all the people who contributed to its success.

\vspace{1cm}

\begin{flushright}
  \parbox{0.7\textwidth}{
    \begin{center}
      \textit{Mauro Morandin}
      \\\textit{Former CNAF Director}
    \end{center}
  }
\end{flushright}


\cleardoublepage

\phantomsection
\addcontentsline{toc}{chapter}{Valerio Venturi (1975--2013)}
\markboth{}{}
\chapter*{Valerio Venturi (1975--2013)}
\thispagestyle{plain}

\begin{wrapfigure}{l}{0.4\textwidth}
  \begin{center}
    \includegraphics[width=0.38\textwidth]{papers/valerio.jpg}
  \end{center}
%  \caption{Valerio Venturi (1975--2013)}
\end{wrapfigure}

Our colleague and friend Valerio Venturi suddenly passed away on
Christmas day while having lunch with his family.

Valerio was born in Florence on April, 5th 1975. He studied
Mathemathics at the University of Florence, and graduated in 2003
discussing a thesis on algebraic topology in symplectic manifolds.
After the thesis, he obtained a MsC in applied mathematics at the
University of Bologna, focusing on the study of invariant signatures
for planar shape recognition.

In May 2004 Valerio joined INFN-CNAF as a member of the VOMS
development team. His contribution in this role was of great
importance for enstabilishing VOMS as the core of the Grid middleware
authorization stack. He first worked on the implementation of the VOMS
server, clients and APIs and then focused on VOMS integration in SAML
federations.

From 2008 to 2010 Valerio led the INFN team that worked in the ETICS-2
project. Besides representing INFN in the technical board of the
project, Valerio was responsible for the project work-package in
charge of handling support request from users of the infrastructure,
helping new customers integrating their projects with the ETICS
system.

After a brief work experience in Lepida, in late 2011 he returned to
INFN to lead the development group of the Italian Grid Infrastructure
project. His responsibility was to ensure that the middleware that
runs the distributed storage and computing infrastructure supporting
the LHC experiments and other scientific communities worked reliably
and evolved to meet emerging new requirements.

Valerio was a delightful person, with a great vision and a natural
ability to relate with people. He will be dearly missed for his
valuable contributions at work, but above all for his friendship and
fine humour.

% include a part entry in the toc
\newcommand{\ip}[1]{%includepart
  \cleardoublepage
  \thispagestyle{empty}
  \phantomsection
%  \addtocontents{toc}{\string\begin{NoHyper}}
  \addcontentsline{toc}{part}{#1}
%  \addtocontents{toc}{\string\end{NoHyper}}
  \addtocontents{toc}{\protect\mbox{}\protect\hrulefill\par}
  {\null\hfill\LARGE\textbf{#1}}
  \cleardoublepage
}

% for each paper:
% * add a directive to include the paper in the toc
%   (\phantomsection makes \addcontentsline work well with hyperref)
% * include the first page with a plain page style
%   (=> no heading and footer)
% * set the left marker of the heading
% * include the rest of the paper with the fancy page style

\newcommand{\ia}[2]{%includearticle
  \phantomsection
  \addcontentsline{toc}{chapter}{#1}
  \includepdf[pages=1, pagecommand={\thispagestyle{plain}}]{papers/#2}
  \markboth{#1}{}
  \includepdf[pages=2-, pagecommand={\thispagestyle{fancy}}]{papers/#2}
}

\ip{Scientific Exploitation of CNAF ICT Resources}

\ia{The CNAF User Support Service}{user_support}
\ia{The ALICE experiment activities at CNAF}{alice}
\ia{ATLAS activities}{atlas}
\ia{Pierre Auger Observatory Data Simulation and Analysis at CNAF}{auger}
\ia{The BaBar Experiment at the INFN CNAF Tier1}{babar}
\ia{The Belle II Experiment at the INFN CNAF Tier1}{belle2}
\ia{The Borexino experiment: recent scientific results and CNAF resources usage}{borexino}
\ia{CDF computing at CNAF}{cdf}
\ia{The CMS Experiment at the INFN CNAF Tier1}{cms}
\ia{The Cherenkov Telescope Array}{cta}
\ia{The Fermi -LAT Grid Interface to CNAF}{glast}
\ia{The ICARUS Experiment}{icarus}
\ia{Kloe data management at CNAF}{kloe}
\ia{The KM3NeT detector}{km3net}
\ia{LHCb Computing at CNAF}{lhcb}
\ia{The PAMELA experiment}{pamela}
\ia{The SuperB project at the INFN CNAF Tier1}{superb}
\ia{The Virgo experiment. Report for the CNAF}{virgo}
\ia{Xenon computing activities}{xenon}

% to keep together the next part title with its chapters in the toc
%\addtocontents{toc}{\newpage}

\ip{The INFN-Tier1 Center and National ICT Services}

\ia{The INFN-Tier1: a general introduction}{t1-intro}
\ia{The INFN-Tier1: infrastructure facilities}{t1-infra}
\ia{The INFN-Tier1: Network}{t1-net}
\ia{The INFN-Tier1: Data management}{t1-data}
\ia{The INFN-Tier1: the computing farm}{t1-farm}
\ia{National ICT infrastructures and services}{servizi_nazionali}


\ip{Software Services and Distributed Systems}

\ia{Software development made easier}{isss}
\ia{The Trigger and Data Acquisition system of the KM3NeT-Italy detector}{tridas}
\ia{The COKA Project}{coka}
\ia{Quality in Software for Distributed Computing}{sw_quality}
\ia{WNoDeS: a virtualization framework in continuing evolution}{wnodes}
\ia{CNAF activities in the MarcheCloud project}{marche_cloud}
\ia{EMI Testbed Improvements and Lessons Learned from the EMI 3 Release}{emi_testbed}
\ia{Accessing Grid and Cloud Services through a Scientific Web portal}{igi_portal}
\ia{Grid Operation Service}{grid_ops}
\ia{Middleware support, maintenance and development}{mw}


\ip{Additional Information}

\cleardoublepage % force a right-side page

\phantomsection
\addcontentsline{toc}{chapter}{Organization}
\markboth{\MakeUppercase{Organization}}{\MakeUppercase{Organization}}
\chapter*{Organization}
\thispagestyle{plain}

\vspace*{1cm}

\subsection*{Director}

\begin{tabular}{ l l }
Mauro Morandin & \textit{\small (till September $30^{th}$)}\\
Gaetano Maron & \textit{\small (since October $1^{st}$)}
\end{tabular}

\subsection*{Scientific Advisory Panel}

\begin{tabular}{ l l p{7cm} }
\textit{Chairperson {\small(till September $30^{th}$)}} & Gaetano Maron & \textit{\small INFN -- Laboratori Nazionali di Legnaro, Italy} \\
\textit{Chairperson {\small(since October $1^{st}$)}}   & Michael Ernst & \textit{\small Brookhaven National Laboratory, USA} \\
& Gian Paolo Carlino & \textit{\small INFN -- Sezione di Napoli, Italy} \\
& Patrick Fuhrmann & \textit{\small Deutsches Elektronen-Synchrotron, Germany} \\
& Josè Hernandez & \textit{\small Centro de Investigaciones Energéticas, Medioambientales y Tecnológicas, Spain} \\
& Donatella Lucchesi & \textit{\small Università di Padova, Italy} \\
& Vincenzo Vagnoni & \textit{\small INFN -- Sezione di Bologna, Italy}

\end{tabular}

% open local environment where the format of section and subsection
% is modified
{

% see titlesec documentation
%\titleformat{ command }[ shape ]{ format }{ label }{ sep }{ before-code }[ after-code ]
\titleformat{\section}{\large\bfseries\center}{}{}{}[\titlerule]
\titleformat{\subsection}{}{}{}{{\bfseries Head:} }

%\titlespacing*{ command }{ left }{ before-sep }{ after-sep }[ right-sep ]
%\titlespacing*{\section}{0pt}{3.5ex plus 1ex minus .2ex}{2.3ex plus .2ex}

% NB use the * versions of section and subsection otherwise they end up in the toc

\section*{User Support}

\subsection*{C. Grandi}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  M. Tenti    & L. Morganti  & S. A. Tupputi & S. Taneja
\\S. Virgilio & S. Perazzini & P. Franchini

\end{tabular}

\section*{Tier1}

\subsection*{L. dell'Agnello}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  \textbf{Farming}              & \textbf{Storage}     & \textbf{Networking} & \textbf{Infrastructure}
\\[.1cm]\underline{A. Chierici} & \underline{P. Ricci} & \underline{S. Zani} & G. Bortolotti
\\S. Dal Pra                    & A. Cavalli           & A. Barilli          & A. Ferraro
\\M. Donatelli                  & M. Favaro            & L. Chiarelli        & A. Mazza
\\F. Rosso                      & D. Gregori           & D. De Girolamo      & M. Onofri
\\A. Simonetto                  & B. Martelli          & G. Giotta
\\                              & M. Pezzi     
\\                              & A. Prosperini
\\                              & V. Sapunenko 
\\                              & G. Zizzi     
\end{tabular}

\section*{R\&D Service}

\subsection*{D. Salomoni}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  M. Caberletti & V. Ciaschini & F. Giacomini & M. Manzali
\\M. Perlini    & E. Ronchieri 

\end{tabular}

\section*{National ICT Services}

\subsection*{R. Veraldi}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

S. Antonelli & S. Longo

\end{tabular}

\section*{Grid Services}

\subsection*{M. C. Vistoli}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  D. Andreotti    & M. Bencivenni & F. Capannini  & A. Ceccanti
\\M. Cecchi       & D. Cesini     & A. Cristofori & G. Dalla Torre
\\M. Di Benedetto & E. Fattibene  & T. Ferrari    & D. Michelotto 
\\G. Misurelli    & A. Paolini    & V. Venturi    & P. Veronesi
\\E. Vianello

\end{tabular}

\section*{Hardware and Software Support}

\subsection*{G. Vita Finzi}

\section*{Information System}

\subsection*{G. Guizzunti}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  S. Bovina & S. Cattabriga & E. Capannini  & M. Canaparo
\\C. Galli  & C. Simoni

\end{tabular}

\section*{Administration}

\subsection*{M. Pischedda}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth}}

A. Aiello & G. Grandi & A. Marchesi

\end{tabular}

} % close local environment


\cleardoublepage % force a right-side page

\phantomsection
\addcontentsline{toc}{chapter}{Seminars}
\markboth{\MakeUppercase{Seminars}}{\MakeUppercase{Seminars}}
\chapter*{Seminars}
\thispagestyle{plain}

\begin{longtable}{ l p{12cm} }

  Feb.  $1^{st}$, 2013 & Francesco Giacomini\\
  & \textbf{The New C++ Standard (The Library)} \\[.5cm] % leave .5 cm of vertical space

  Feb. $13^{th}$, 2013 & Pier Paolo Deminicis \\
  & \textbf{La costruzione di un business plan e le problematiche sulla creazione d’impresa} \\[.5cm]

  Feb. $14^{th}$, 2013 & Fabrizio Furano \\
  & \textbf{Storage Federations In Ambito HEP} \\[.5cm]

  Feb. $22^{nd}$, 2013 & Andrea Negri \\
  & \textbf{Hierarchical Data Format 5 (HDF5): Why and How to Use It} \\[.5cm]

  May $10^{th}$, 2013 & Davide Salomoni \\
  & \textbf{Hype in the Cloud, Stacks in the Ground - Andata e Ritorno per il Cloud Computing} \\[.5cm]

  May $17^{th}$, 2013 & Stefano Spataro \\
  & \textbf{Computing Design Choices for the PANDA Experiment} \\[.5cm]

  May $22^{nd}$, 2013 & Charles Loomis \\
  & \textbf{Scientific Cloud Computing - Present and Future} \\[.5cm]

  Jun.  $7^{th}$, 2013 & Michele Pezzi, Paolo Veronesi \\
  & \textbf{Installazione e Configurazione di un Centro di Calcolo con Puppet e Clobber} \\[.5cm]

  Jun. $26^{th}$, 2013 & Giuseppe Misurelli \\
  & \textbf{Sursum Log: Introduzione a OSSEC e Splunk} \\[.5cm]

  Jul. $17^{th}$, 2013 & Elisabetta Ronchieri \\
  & \textbf{A Software Quality Predictive Model} \\[.5cm]

  Oct.  $1^{st}$, 2013 & Salvatore Alessandro Tupputi \\
  & \textbf{Automating Usability of ATLAS Distributed Computing Resources} \\[.5cm]

  Oct. $23^{rd}$, 2013 & Tim Mattson \\
  & \textbf{Predicting the Future in a Rapidly Changing Many-core World} \\[.5cm]

  Oct. $28^{th}$, 2013 & \textbf{Report da CHEP 2013} \\[.5cm]

  Nov. $20^{th}$, 2013 & Davide Salomoni, Paolo Veronesi \\
  & \textbf{Report da OpenStack Summit 2013} \\[.5cm]

  Nov. $21^{st}$, 2013 & Rosa Brancaccio \\
  & \textbf{Tomografia computerizzata e software parallelo di ricostruzione} \\[.5cm]

  Dec.  $3^{rd}$ -- $4^{th}$, 2013 & \textbf{CUDA/OpenACC Workshop} \\[.5cm]

  Dec. $18^{th}$, 2013 & Andrea Petrucci, CERN \\
  & \textbf{XDAQ: A Software Framework for Distributed Trigger and Data Acquisition Systems in HEP Experiments}

\end{longtable}


\cleardoublepage % force a right-side page

\thispagestyle{empty}
\phantomsection
\addcontentsline{toc}{part}{50 Years of CNAF}
\addtocontents{toc}{\protect\mbox{}\protect\hrulefill\par}

\null\vfill
\begin{center}    
  \includegraphics{papers/cnaf50.jpg}
\end{center}
\vfill
~

\newpage

\thispagestyle{empty}

\null\vfill

\subsection*{Authors of the texts}

Antonia Ghiselli\\
Pietro Matteuzzi\\
Mauro Morandin\\
Cristina Vistoli

\subsection*{With contributions by}

Lorenzo Chiarelli\\
Francesco Giacomini\\
Luca dell'Agnello\\
Davide Salomoni\\
Stefano Zani\\
Umberto Zanotti

\subsection*{$50^{th}$ anniversary event}

\textit{agenda.cnaf.infn.it/event/CNAF50}


\cleardoublepage % force a right-side page

\ia{Bubble chambers and the \textit{Flying Spot Digitizer}}{bubble}
\ia{The birth of INFNet}{infnet}
\ia{Connecting Italian research networks: GARR}{garr}
\ia{The computing infrastructure of LHC}{computing_lhc}
\ia{The computing infrastructure of Grid}{computing_grid}

\cleardoublepage

\end{document}
