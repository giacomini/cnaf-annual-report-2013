#!/bin/sh

dn=$(dirname $0)
if [ "${dn}" = "." ]; then
  echo "Run the build in a subdirectory or in an out-of-source directory"
  exit 1
fi

topdir=$(readlink -f ${dn})
builddir=$(readlink -f .)
papersdir=${builddir}/papers

if [ ! -d ${papersdir} ]; then
    mkdir -p ${papersdir}
fi

ln -sf ${topdir}/contributions/user_support/AR2014-UserSupport.pdf       ${papersdir}/user_support.pdf
ln -sf ${topdir}/contributions/atlas/ATLAS-report-CNAF-201403-2.pdf      ${papersdir}/atlas.pdf
ln -sf ${topdir}/contributions/auger/AugerARperCNAF_1.pdf                ${papersdir}/auger.pdf
ln -sf ${topdir}/contributions/babar/CNAFBaBar.pdf                       ${papersdir}/babar.pdf
ln -sf ${topdir}/contributions/belle2/CNAFBelle2.pdf                     ${papersdir}/belle2.pdf
ln -sf ${topdir}/contributions/borexino/borexino_AnnualReport.pdf        ${papersdir}/borexino.pdf
ln -sf ${topdir}/contributions/cms/REPORT\ CMS\ CNAF.pdf                 ${papersdir}/cms.pdf
ln -sf ${topdir}/contributions/cta/CNAF.pdf                              ${papersdir}/cta.pdf
ln -sf ${topdir}/contributions/km3net/CNAF_2014_km3net.pdf               ${papersdir}/km3net.pdf
ln -sf ${topdir}/contributions/lhcb/LHCbCNAFAnnualReport2014Last.pdf     ${papersdir}/lhcb.pdf
ln -sf ${topdir}/contributions/superb/CNAFSuperB.pdf                     ${papersdir}/superb.pdf
ln -sf ${topdir}/contributions/virgo/VIRGOatCNAF.pdf                     ${papersdir}/virgo.pdf
ln -sf ${topdir}/contributions/xenon/AR2014-Xenon.pdf                    ${papersdir}/xenon.pdf
ln -sf ${topdir}/contributions/t1/Tier1-intro-2013-annualreport.pdf      ${papersdir}/t1-intro.pdf
ln -sf ${topdir}/contributions/t1/Tier1-infra-2013-annualreport_rev1.pdf ${papersdir}/t1-infra.pdf
ln -sf ${topdir}/contributions/t1/Tier1-net-2013-annualreport-1.pdf      ${papersdir}/t1-net.pdf
ln -sf ${topdir}/contributions/t1/Tier1-data-2013-annualreport.pdf       ${papersdir}/t1-data.pdf
ln -sf ${topdir}/contributions/t1/Tier1-farm-2013-annualreport-1.pdf     ${papersdir}/t1-farm.pdf
ln -sf ${topdir}/contributions/marche_cloud/marcheCloud.pdf              ${papersdir}/marche_cloud.pdf
ln -sf ${topdir}/contributions/grid_ops/AR2013-grid.pdf                  ${papersdir}/grid_ops.pdf
ln -sf ${topdir}/contributions/valerio/valerio.jpg                       ${papersdir}/valerio.jpg
ln -sf ${topdir}/contributions/cnaf50/Logo-50esimo_tagliato.jpg          ${papersdir}/cnaf50.jpg

cd ${builddir}

# build cover
if [ ! -d cover ]; then
    mkdir cover && cd cover

    convert -verbose ${topdir}/contributions/cover/ar_ok-01.tif cover.pdf
    ln -s ${builddir}/cover/cover.pdf ${papersdir}/cover.pdf

    cd ..
fi

# build alice contribution from source
if [ ! -d alice ]; then
    mkdir alice
    cd alice

    cp ${topdir}/contributions/alice/AR2014-ALICE.tex .
    cp ${topdir}/contributions/alice/ALICE-CPU-time.png .
    cp ${topdir}/contributions/alice/ALICE-active-jobs.png .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex AR2014-ALICE
    pdflatex AR2014-ALICE
    pdflatex AR2014-ALICE
    ln -sf ${builddir}/alice/AR2014-ALICE.pdf ${papersdir}/alice.pdf

    cd ..
fi

# build cdf contribution from source
if [ ! -d cdf ]; then
    mkdir cdf
    cd cdf

    cp ${topdir}/contributions/cdf/CDF_CNAF_annual_report.tex .
    cp -a ${topdir}/contributions/cdf/figures .
    cp ${topdir}/contributions/cdf/cdfbib.bib .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    unzip BibTeX/iopart-num.zip
    mv iopart-num/iopart-num.bst .
    pdflatex CDF_CNAF_annual_report
    bibtex CDF_CNAF_annual_report
    pdflatex CDF_CNAF_annual_report
    pdflatex CDF_CNAF_annual_report
    ln -sf ${builddir}/cdf/CDF_CNAF_annual_report.pdf ${papersdir}/cdf.pdf

    cd ..
fi

# build sw quality contribution from source
if [ ! -d sw_quality ]; then
    mkdir sw_quality
    cd sw_quality

    cp ${topdir}/contributions/sw_quality/* .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    unzip BibTeX/iopart-num.zip
    mv iopart-num/iopart-num.bst .
    pdflatex ReportQuality
    bibtex ReportQuality
    pdflatex ReportQuality
    pdflatex ReportQuality
    ln -sf ${builddir}/sw_quality/ReportQuality.pdf ${papersdir}/sw_quality.pdf

    cd ..
fi

# build wnodes contribution from source
if [ ! -d wnodes ]; then
    mkdir wnodes
    cd wnodes

    cp ${topdir}/contributions/wnodes/ReportWNoDeS.tex .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex ReportWNoDeS
    pdflatex ReportWNoDeS
    pdflatex ReportWNoDeS
    ln -sf ${builddir}/wnodes/ReportWNoDeS.pdf ${papersdir}/wnodes.pdf

    cd ..
fi

# build EMI testbed contribution from source
if [ ! -d emi_testbed ]; then
    mkdir emi_testbed
    cd emi_testbed

    cp ${topdir}/contributions/emi_testbed/EMItestbed.tex .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex EMItestbed
    pdflatex EMItestbed
    pdflatex EMItestbed
    ln -sf ${builddir}/emi_testbed/EMItestbed.pdf ${papersdir}/emi_testbed.pdf

    cd ..
fi

# build glast contribution from source
if [ ! -d glast ]; then
    mkdir glast
    cd glast

    tar xf ${topdir}/contributions/glast/cnaf_2014_v8_toCNAF.tar
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex cnaf_2014_v8
    pdflatex cnaf_2014_v8
    pdflatex cnaf_2014_v8
    ln -sf ${builddir}/glast/cnaf_2014_v8.pdf ${papersdir}/glast.pdf

    cd ..
fi

# extract the pdf from the pamela zip
if [ ! -d pamela ]; then
    mkdir pamela
    cd pamela

    unzip ${topdir}/contributions/pamela/PAMELA_CNAF_report_2014.zip
    ln -sf ${builddir}/pamela/PAMELA_CNAF_report_2014.pdf ${papersdir}/pamela.pdf

    cd ..
fi

# extract the pdf from the kloe zip
if [ ! -d kloe ]; then
    mkdir kloe
    cd kloe

    tar jxf ${topdir}/contributions/kloe/kloe_annualreport2013.tar.bz2
    ln -sf ${builddir}/kloe/kloe_annualreport2013/kloe_cnaf.pdf ${papersdir}/kloe.pdf

    cd ..
fi

# build isss contribution from source
if [ ! -d isss ]; then
    mkdir isss
    cd isss

    cp ${topdir}/contributions/isss/* .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex isss
    pdflatex isss
    pdflatex isss
    ln -sf ${builddir}/isss/isss.pdf ${papersdir}/isss.pdf

    cd ..
fi

# build tridas contribution from source
if [ ! -d tridas ]; then
    mkdir tridas
    cd tridas

    cp ${topdir}/contributions/tridas/tridas.tex .
    cp ${topdir}/contributions/tridas/nemo.jpg .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex tridas
    pdflatex tridas
    pdflatex tridas
    ln -sf ${builddir}/tridas/tridas.pdf ${papersdir}/tridas.pdf

    cd ..
fi

# build coka contribution from source
if [ ! -d coka ]; then
    mkdir coka
    cd coka

    tar zxf ${topdir}/contributions/coka/report-coka.tgz
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex report-coka
    pdflatex report-coka
    pdflatex report-coka
    ln -sf ${builddir}/coka/report-coka.pdf ${papersdir}/coka.pdf

    cd ..
fi

# build servizi_nazionali contribution from source
if [ ! -d servizi_nazionali ]; then
    mkdir servizi_nazionali
    cd servizi_nazionali

    cp ${topdir}/contributions/servizi_nazionali/servizi_nazionali.tex .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex servizi_nazionali
    pdflatex servizi_nazionali
    pdflatex servizi_nazionali
    ln -sf ${builddir}/servizi_nazionali/servizi_nazionali.pdf ${papersdir}/servizi_nazionali.pdf

    cd ..
fi

# build IGI Portal contribution from source
if [ ! -d igi_portal ]; then
    mkdir igi_portal
    cd igi_portal

    cp ${topdir}/contributions/igi_portal/* .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex IGIPortal-Annual-Report-2013
    bibtex IGIPortal-Annual-Report-2013
    pdflatex IGIPortal-Annual-Report-2013
    pdflatex IGIPortal-Annual-Report-2013
    ln -sf ${builddir}/igi_portal/IGIPortal-Annual-Report-2013.pdf ${papersdir}/igi_portal.pdf

    cd ..
fi

# build icarus contribution from source
if [ ! -d icarus ]; then
    mkdir icarus
    cd icarus

    cp ${topdir}/contributions/icarus/CNAF_Report2014_ICARUS.tex .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex CNAF_Report2014_ICARUS
    pdflatex CNAF_Report2014_ICARUS
    pdflatex CNAF_Report2014_ICARUS
    ln -sf ${builddir}/icarus/CNAF_Report2014_ICARUS.pdf ${papersdir}/icarus.pdf

    cd ..
fi

# build cnaf 50 contributions from scratch
if [ ! -d cnaf50 ]; then
    mkdir cnaf50
    cd cnaf50

    cp ${topdir}/contributions/cnaf50/*.tex .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex bubble
    pdflatex infnet
    pdflatex garr
    pdflatex computing_lhc
    pdflatex computing_grid
    ln -sf ${builddir}/cnaf50/bubble.pdf ${papersdir}/bubble.pdf
    ln -sf ${builddir}/cnaf50/infnet.pdf ${papersdir}/infnet.pdf
    ln -sf ${builddir}/cnaf50/garr.pdf ${papersdir}/garr.pdf
    ln -sf ${builddir}/cnaf50/computing_lhc.pdf ${papersdir}/computing_lhc.pdf
    ln -sf ${builddir}/cnaf50/computing_grid.pdf ${papersdir}/computing_grid.pdf

    cd ..
fi

# build mw contribution from source
if [ ! -d mw ]; then
    mkdir mw
    cd mw

    cp ${topdir}/contributions/mw_devel/mw_2013.tex .
    unzip ${topdir}/istruzioni/LaTeXTemplates.zip
    pdflatex mw_2013
    pdflatex mw_2013
    pdflatex mw_2013
    ln -sf ${builddir}/mw/mw_2013.pdf ${papersdir}/mw.pdf

    cd ..
fi

pdflatex ${topdir}/ar.tex
pdflatex ${topdir}/ar.tex
pdflatex ${topdir}/ar.tex
