\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}
\title{The Virgo experiment. Report for the CNAF}

\author{Pia Astone, Data analysis coordinator of the Virgo experiment and 
Alberto Colla, Virgo contact person at CNAF.}

\address{Virgo experiment: {\tt https://wwwcascina.virgo.infn.it/}}

\ead{pia.astone@roma1.infn.it,alberto.colla@roma1.infn.it}

\begin{abstract}
We give here  a general description of the computing strategy of the Advanced Virgo (AdV) project, based on the experience and work done for the Virgo project.
We will focus in particular on the computing aspects related to the use 
of resources at CNAF, to show how the usage of CNAF has and will 
be important for our searches. 
\end{abstract}

\section{Introduction: description of the project}
The Virgo experiment is based on a kilometer-scale gravitational wave 
detector, which has taken scientific data from  May 2007 to Sept. 2011, 
over four runs (VSR1, 2, 3, 4).
It is part of a wide network of detectors, so far with the two LIGO detectors 
at Hanford and Livingston. In the future the network of advanced detectors 
will include  also the Japanese Kagra detector and farther on the horizon 
the IndIGO Indian detector.
The first generation runs have been completed and the detectors 
have been shut down to undergo major upgrades aimed at reaching a much improved sensitivity.
The new project is named ``Advanced Virgo'' (AdV)  detector and the expected sensitivity, together with prospects for the detection of gravitational waves (GW), have been reported in \cite{observing}.
The analysis of the first generation detector data for many different searches has been done and results published, while other analysis are still ongoing.
We are now taking the opportunity of the transition from first to second 
generation experiments (AdV and aLIGO) to improve the computing framework 
of the experiment, when needed. 
The new Computing Model (CM) has been written with
the experience done with Virgo and trying to enhance the usage of 
our internal resources. 

\section{Major past accomplishments}
As said, many analysis on the data of first generation detector have been completed and some (not less important)
are still ongoing. In particular the searches for continuous persistent signals, like those emitted from unknown isolated neutron stars, are still ongoing as they require long integration time in order to increase the signal-to-noise ratio and huge computational resources to explore a large parameter space.
So far, we have not detected GW., as expected given the sensitivities, run time and theoretical predictions \cite{observing}. But the results we have obtained are indeed important, not only as we have gained experience by learning 
how to deal with real data and how to optimize data analysis algorithms, 
but also as the upper limits we have put in some cases do constraint the physical parameters of possible sources. 
The scientific goals have been divided into four main groups, reflecting the features of expected signals: 
CBC (search for compact binary coalescences), Burst (search for unmodeled  transients), CW (search for continuous waves), STOCH (search for stochastic background of cosmological or astrophysical origin).
In the following there is a selection of some recent results, to give an idea of the different GW searches accomplished. 
The titles of the papers are quite explicative of the scientific goal behind the search.
\begin{itemize}
\item
``Search for Gravitational Waves from Low Mass Compact Binary Coalescence in LIGO's 
Sixth Science Run and Virgo's Science Runs 2 and 3''  Phys. Rev. D 85, 082002 (2012) 
\item
`` Parameter Estimation for Compact Binary Coalescence Signals with the First Generation Gravitational-Wave Detector Network ``. Phys. Rev. D 88, 062001 (2013)
\item
 ``Search for Gravitational Waves from Binary Black Hole Inspiral, Merger and Ringdown in LIGO-Virgo data from 2009-2010 ''. Phys. Rev. D 87, 022002 (2013) 
\item
``All-sky Search for Gravitational-Wave Bursts in the Second Joint LIGO-Virgo Run ``
Phys. Rev. D 85, 122007 (2012) 
\item
``Search for long-lived gravitational-wave transients coincident with long gamma-ray bursts''   Phys. Rev. D 88, 122004 (2013)  
\item
``First Searches for Optical Counterparts to Gravitational-Wave Candidate Events ``
Astrophys. J. Supp. 211, 7 (2014) 
\item
``Constraints on cosmic (super)strings from the LIGO-Virgo gravitational-wave detectors `` to appear in PRL
\item
`` Gravitational waves from known pulsars: results from the initial detector era ``
To appear in Astrophysica Journal.
 %Here we have presents the results of searches for gravitational waves from a 
%large selection of pulsars using data from the most recent science 
%runs (S6, VSR2 and VSR4) of the initial generation of interferometric 
%gravitational wave detectors LIGO and Virgo. 
%For the Crab and Vela pulsars we have surpassed their spin-down limits. 
\item
``Implementation of an F-statistic all-sky search for continuous gravitational waves in Virgo VSR1 data `` Submitted to CQG 
\end{itemize}
\section{The overall computing strategy}
The overall computing strategy for Advanced Virgo has been described in the 
AdV Computing Model \cite{CM2013} and details on the technical implementation 
are being described in the Implementation Plan \cite{IP2014}.
%One of the main goals of the CM is to define a production and analysis
%system able to guarantee an access to data and resources transparent to the
%end users.
Virgo (and AdV) has a hierarchical model for data production and distribution:
different kinds of data are produced by the detector and firstly 
stored at the EGO site in Cascina, where the detector is. 
There is no permanent data storage in Cascina but we foresee to
install a disk buffer of 6 months of data acquisition for local 
access.
The external CCs receive a copy of the data and
provide storage resources for permanent data archiving. 
They must guarantee fast data access  and computing resources 
for off-line analyses. 
Finally, they must provide the network links to the other 
AdV computing resources.
For this goal a robust data distribution and access framework 
(based on file and metadata catalogs) is a crucial point.

The collaboration manages also smaller CCs used to run part of some 
analyses, simulations or for software developments and tests.
% in which the CM
%does not foresee specific data transfer and access frameworks. 

During science runs the Cascina facility is dedicated to data production and to 
different detector characterization and commissioning analysis, 
which have the need to run ``on-line''
(with a very short latency, from seconds to minutes, 
to give rapid information on the quality of the data) 
or ``in-time'' (with a higher latency, even hours, but which again produce 
information on the quality of the data within a well defined time scale). 
The detector characterization activity gives support to both commissioning and 
science analysis.

Science analyses are carried out only off-line at the external CCs, 
with the only exception of  the low-latency searches. 
Low-latency searches are run with a very small latency after the data taking, of the order of minutes, to provide alerts to EM partners. Thus this analysis runs in Cascina, at the EGO site. 
The same analysis can be then repeated offline, using CNAF or CCIN2P3 resources, to refine the explored parameter
space or to use a new version of calibrated data of the detector.
Some analyses, due to the fact that we analyze data jointly with 
aLIGO for many searches, are carried in LSC CCs.

To face the huge computational demands of GW
searches in the advanced detectors era (ADE),  there will be the need 
to gather the resources of many CCs into a homogeneous 
distributed environment  (like Grids and/or Clouds ) and 
to adapt the science pipelines to run under such distributed
environment.

Another very important need for ADE is to provide a Grid-enabled, 
aLIGO-compatible Condor cluster for AdV people.
In fact one bottleneck that has been 
identified in the past years was the difficulty to run 
at CNAF and CCIN2P3, which have been the Virgo external 
Computing Centers (CCs) from the very beginning, pipelines that were initially 
developed  to run on LSC clusters and had shown a tight  dependency 
on their architecture (Condor based submissions).
The first attempt (2011/2012) to face these problems was the 
implementation of a submission system at CNAF based on the pilot job framework, 
which creates a virtual Condor cluster on a Virgo farm to which 
jobs can be submitted from a user interface. 
Another alternative which we have more recently (2013) succesfully tested 
at CNAF is the Pegasus Workflow Management System [http://pegasus.isi.edu/], 
which provides a layer for the job submission in different Grid environments.
%Of course, the various computing and storage tasks have to be 
%coordinatedly distributed over these CCs. 

Another important task, which we started to face, is the possibility 
in ADE to run some search pipelines in GPU clusters.  
%For this goal we started to translate part of our codes in the 
%OpenCL language, which allows transparent execution on CPUs or GPUs


Most GW searches require the use of a network of detectors 
(at least AdV and aLIGO). As a consequence, these search pipelines must be able to run either in AdV or aLIGO CCs.
It is therefore important to develop pipelines adaptable to 
different environments or interfaces which hide the different 
technologies to the users.

Thus the most important issues of the AdV CM may be summarized as follows:
\begin{itemize}
\item
guarantee adequate storage and computing resources at Cascina, 
for commissioning, detector characterization and low-latency searches;
\item
guarantee fast communications between Virgo applications at Cascina and 
aLIGO CCs/other detectors for low-latency searches;
\item
guarantee reliable storage and computing resources for off-line analyses 
in the AdV CCs;
\item
push towards the use of geographically distributed resources (Grid/Cloud), 
in external CCs and whenever appropriate;
\item
push towards a homogeneus model for data distribution, bookkeping and 
access.
\end{itemize}

Figure \ref{SCHEMAS} gives a big picture of the data workflow for what concerns 
scientific data analysis (DA) and detector characterization (Detchar) activities for AdV. 
In the picture CC2 indicates the CNAF, CC1 indicates CCIN2P3. Possible additional CCs have also been indicated, as a resource to perform intensive data analysis computation on the most important scientific data channels 
(which amounts to a really negligible storage need/year). 
\begin{figure}[ht!]
\centering
\framebox{
\includegraphics[width=90mm]{SCHEMA.pdf}}
\caption{Data workflow for DA and Detchar activities in AdV. CC2 indicates the CNAF.
CC1 indicates CCIN2P3.}
\label{SCHEMAS}
\end{figure} 

\section{The role of CNAF}
The computing usage and needs at CNAF is described in internal documentation which 
we prepare at the end of each year, to plan the needed resource for the next year\cite{VCN2013,VCN2014}.

Over the last year CNAF has mainly been used for:
\begin{itemize}
\item
Parameter estimation and General Relativity tests by the CBC group
\item
Science data preconditioning work by the Burst and Noise studies group
\item
All-sky searches for unknown isolated neutron stars by the CW group
\item
Narrow-band searches for isolated neutron stars by the CW group
\item
Optimazion studies for the CBC low-latency pipeline
\end{itemize}
Most of these use the GRID.
\subsection{Storage}
Table \ref{tabSTCNAF} shows the storage at CNAF by the year 2009 up to the end of 2013.

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
\hline
Year CNAF & gpfs4 [TB] & gpfs3 [TB] & Castor or & Castor disk [TB] \\
 & used / available Virgo & used / available Virgo &  GEMSS [TB]     & used / available all exp.\\
\hline
2009            & 190 / 256           & 9 / 16    & 145 (Castor)       & (+)\\
2010 (Oct. 1)   & 261 / (256+186)=442 & 16 / 16   & 163 (Castor)       & 17 / 36\\
2011            & 345 / 384           & 26 / 32   & 750            & 0\\
2012 (Oct. 29)  & 325 / 368           & 33 / 48   & 826                & 0\\
2013 (Nov. 18)  & 254/ 379          & 67 / 48   & 826               & 0\\
\hline
\hline
\end{tabular}
\caption{\label{tabSTCNAF}
Storage at CNAF since 2009. (+) means that we don't know the exact number. In 2011 data 
from Castor have migrated to GEMSS, which uses gpfs\_virgo4 as cache disk. 
}
\end{center}
\end{table}

\subsection{Computing}
CNAF accounting system is providing\footnote{http://tier1.cnaf.infn.it/monitor} information in 
wct\_hep\_day and cpt\_hep\_day\footnote{1 hep\_day = 1 HS06.day}. 
Current computing consumption at CCIN2P3 are 
reported\footnote{http://cctools.in2p3.fr/mrtguser/info\_sge\_rqs.php?group=virgo} 
in HSE06.hours (CPU time not wall clock time). 
 Current consumption for 2013 is given below, while Table \ref{tabCC} shows the 
evolution since 2007 of the CPU consumptions.
We mainly use ``Wall-clock time'' as this is the quantity we use to account to CNAF.
\begin{itemize}

\item  {\bf CNAF (date: January, 1st - November, 20 2013)}

%\begin{verbatim}     ------2012-------------
%Wall clock time (wct_hep_day):	89642.8 (Total)   295.85 (Average)
%CPU time (cpt_hep_day):	47122.208 (Total)   159.736 (Average)
%\end{verbatim}

\begin{verbatim}
Wall clock time (kHS06.day):	 770 (Total)   
\end{verbatim}
\end{itemize}
\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|}
\hline
year                & CNAF (WCT)         \\
                    & [kHSE06.day]        \\
\hline
2007                &  60               \\
2008                & 240                    \\
2009                & 453                     \\
2010                & 162                     \\
2011                & 674                     \\
2012 		    & 669                    \\
2013                & 770                     \\
\hline
\hline
\end{tabular}
\caption{\label{tabCC}
Evolution since 2007 of the CPU used at the CNAF}
\end{center}
\end{table}

The most intensive usage of CPU resources has been done by all-sky CW search and by the CBC Parameter Estimation and General Relativity work, in preparation of advanced detector era (ADE).

 Some detector characterization work is also performed at CNAF on VSR2 \& VSR3 raw-data set.
Very recently some resources are being used at CNAF to develop new improved features of the CBC low-latency CBC pipeline.

%The request at CNAF for the year 2012 was 400 kHSE06.day. This number, using a conversion factor such that 1 core = 7.5 HS06, corresponds to using 150 cores for 1 year or to using 900 cores for two months. 
%We have used the requested energy in the period  November and December 2012, as CNAF has agreed to assign us O(1000) CPUs for two months.

During the year 2013, an important work has been done to do the porting of  CBC pipelines from an LSC related submission method to an architecture complaint also with our CCs and in particular with GRID. 
This has solved the limit to run CBC analyses only on LSC clusters, opening new possibilities in particular in view of ADE,  to both the Virgo and LSC collaborations.
Tests on real data have begun in September 2013 and to allow them to run soon CNAF has granted 
to Virgo a number of cores O(1000) since September  which was the minimum needed to prepare the CBC analysis in view of ADE and to run some new CW analysis (on VSR2/VSR4 data) enlarging the parameter space covered so far. 
To continue this analysis and to run new searches in the CW group 
we have then made an official request for 1000 cores to be assigned to Virgo 
in the year 2014.

\section{General remarks}
The CNAF   support to the VIRGO experiment  is not limited to the technical access of the CC facilities. The CNAF expertise was crucial for driving the discussion in the collaboration to define our computing model and focus on right solutions.
This kind of support is even more  important than the access to their hardware infrastructure. In addition, CNAF support will be fundamental for testing 
the porting on the GPUs located at CNAF some of most computing demanding searches of gravitational wave signals.
Finally, we note that in the near future of the Advanced detector era, our computing needs will increase: we expect by the year 2018 a need for a continuous power O(100) kHS06, as  we detailed in the CM. This implies that our impact on the CNAF infrastructure  will be still below that  of  the HEP experiments at LHC, but not marginal any more.

%%\end{document}

\section*{References}
\begin{thebibliography}{9}
\bibitem{CM2013} The Virgo collaboration, 2013 {\it The AdV Computing Model} \\Virgo TDS num:"VIR-129E-13"\\
URL   = https://tds.ego-gw.it/ql/?c=10325
\bibitem{IP2014} In preparation, The Virgo collaboration, 2014 {\it The AdV Implementation Plan}. 
TDS number: to be assigned
\bibitem{observing} The LSC and VIRGO collaboration, 2014 {\it Prospects for Localization of Gravitational Wave Transients by the Advanced LIGO and Advanced Virgo Observatories} arXiv:1304.0670
\bibitem{VCN2013}
The Virgo Collaboration,
Virgo computing status and needs for 2013, ''VIR-0413A-12",
url  =https://tds.ego-gw.it/itf/tds/file.php?callFile=VIR-0413A-12.pdf
\bibitem{VCN2014}
The Virgo Collaboration,
Virgo computing status and needs for 2014, ''VIR-0505A-13",
url  =https://tds.ego-gw.it/itf/tds/file.php?callFile=VIR-0505A-13.pdf
\end{thebibliography}



\end{document}


