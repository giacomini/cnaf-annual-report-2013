\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}

\title{Quality in Software for Distributed Computing}

\author{M Canaparo and E Ronchieri and D Salomoni}

\address{INFN CNAF, Viale Berti Pichat 6/2, 40126, Bologna, Italy}

\ead{marco.canaparo@cnaf.infn.it, elisabetta.ronchieri@cnaf.infn.it, davide.salomoni@cnaf.infn.it}

\begin{abstract}
In the last decade INFN CNAF has contributed in developing the majority of software solutions for
distributed computing. It has been participating in several European projects, such as DataGrid,
EGEE, EMI and EGI, which have produced software for achieving the challenges of high energy physics
communities in first place and later for supplying other communities' needs.
Up to now three main areas belonging to the Grid paradigm ascribe to the software products:
storage with StoRM, authorization with VOMS and computing with WMS and WNoDeS.
Aware of the experience done at INFN CNAF, we noticed that software researchers,
who have been implementing this code, massively forget the quality of their products
for different reasons: on the one hand, developers feel pressed for addressing the requests of their
users within scheduled budget and short time; on the other hand, they distrust data from existing
quality tools since they provide partial analysis of their software.
This has led to spend effort maintaining software once released and to develop software
without exploiting solutions for managing defects effectively.
Notwithstanding that developers perceive quality as an extremely time-consuming
task that affects their productivity, in our opinion enhancing quality allows
reducing defects, and, as consequence, saving costs and decreasing delivery delays;
the software quality models and metrics represent the mainstream to reach high reliability
balancing effort and results.

In this report, we are going to describe our solution to the aforementioned issues. Leveraging
past and present literature about software quality models, we designed our own
mathematical model connecting software best practices with code metrics by defining them in a formal way.
To predict the quality at any stage of development, we supplied the model with statistical techniques
such as discriminant analysis and linear regression. Input data to this model are some characteristics
of packages, while outputs are measures of the defined metrics, which build the data set for the
predictive techniques. For the validation process, we used some EMI software products
whose defects were already known. Our solution has proved to reasonably reproduce reality.
\end{abstract}

\section{Current State}
At the beginning of this study, we marked best practices~\cite{CMMI} and metrics~\cite{Kan} suitable for
determining software products success and offering the greatest return. However, we planned the validation
of our model with a progressive increase in the data set to properly speculate on the variables included in the
model.

In~\cite{Ronchieri}, we selected a set of best practices (see~\cite{Perks})
referring to software structure, configuration management, construction of the code and deployment. We derived
from them some metrics further extended with static code ones~\cite{Chidamber}, such as Lines Of Code and
Number of Defects. Once introduced the metrics, we used the mathematical description formalism to express various
levels of abstraction from the fundamental concepts of software engineering up to metrics to design our model.
Ultimately, we supplied it with a predictive technique, called risk-threshold Discriminant Analysis
(DA)~\cite{Guo}, whose starting point is the measure of the foregoing metrics, while its outcomes determine
risky software products that may contain defects. Combining the model with DA needed a validation: therefore
we compared calculated results with the real data coming from EMI releases~\cite{Aiftimiei}. In particular, we
selected source code mainly written in \texttt{Python} and \texttt{sh}, from the
WNoDeS (Worker Nodes on Demand Services)~\cite{Salomoni} and
StoRM (STOrage Resource Manager)~\cite{Zappi} products released in the EMI 3 Monte Bianco distribution,
to highlight similarities and differences among development scenarios. The analysis exploited 1,513 files
in 15 software components amounting to a 27,406 total lines of code by using a Matlab-based prototype tool
that codes the presented solution. The prototype classified all the components in faulty and non-faulty groups
with a correctness of about $83\%$.

In~\cite{Canaparo}, we validated our model enlarging the data set by increasing the number of metrics and
software products. For the former we also considered complexity metrics, while for the latter we used
CREAM (Computing Resource Execution And Management)~\cite{Andreetto},
VOMS (Virtual Organization Management System)~\cite{Ceccanti},
WMS (Workload Management System)~\cite{Cecchi} and
YAIM (Yet Another Installation Manager)~\cite{Jayalal} in addition to StoRM and WNoDeS. Furthermore, we used
as predictive techniques both risk-threshold DA and linear regression~\cite{Fenton} to discriminate the risky
software products and defects respectively. As result, DA confirmed the correctness given in~\cite{Ronchieri},
while the regression method determined an inaccurate number of defects. However, the outcomes can be improved
increasing the data set size and better contextualizing the predictive methods.

\section{Future Work}
Starting from the current state, there are several improvements that we can conduct in the following periods.
In the short-term, we are going to study the correlation among metrics and to express defects as function of
various metrics: the former provides us with details about 
how they influence one another; the latter determines which metric has a greater weight than others. The statistical 
computing tool, called R, represents the best tool to fulfill these achievements.
Our aim is to
identify and improve the predictive technique that reproduces reality as much as possible.
In the medium-term, we would like to increase the data set by adding new software products and metrics both static
and dynamic ones. Our purpose is to improve the $\%$ of correctness in the prediction of our model.
In the long-term, we will consider and adopt further predictive techniques in addition to DA and regression, such
as k-fold cross-validation~\cite{Rodriguez} and support vector method~\cite{Lo}, to strengthen the validation of 
our model.
At this point, our solution will be ready for being adopted by developers and integrated in their development 
environment.

\section*{References}
\bibliographystyle{iopart-num}
\bibliography{report}

\end{document}


