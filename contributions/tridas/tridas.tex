\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{url} 

\begin{document}

\title{The Trigger and Data Acquisition system of the KM3NeT-Italy detector}

\author{
  T Chiarusi$^2$,
  F Giacomini$^1$,
  M Manzali$^1$$^,$$^3$
  and C Pellegrino$^2$
}

\address{$^1$ INFN-CNAF, Bologna, Italy}
\address{$^2$ INFN, Bologna, Italy}
\address{$^2$ Universit\`a degli Studi di Ferrara, Ferrara, Italy}

\ead{
  tommaso.chiarusi@bo.infn.it,
  francesco.giacomini@cnaf.infn.it,
  matteo.manzali@cnaf.infn.it,
  carmelo.pellegrino@bo.infn.it
}

\begin{abstract}
KM3NeT-Italy is an INFN project that will develop a submarine
cubic-kilometre neutrino telescope in the Ionian Sea (Italy) in front
of the south-east coast of Portopalo di Capo Passero, Sicily. It will
use thousands of PMTs to measure the Cherenkov light emitted by
high-energy muons, whose signal-to-noise ratio is quite
disfavoured. This forces the use of an on-line Trigger and Data
Acquisition System (TriDAS) in order to reject as much background as
possible. In March 2013 a prototype detector, hosting 32 PMTs, has been
deployed in the abyssal site of Capo Passero and successfully
operated. The existing TriDAS software, used for the prototype, needs
a deep revision in order to meet the requirements of the final
detector: the adoption of new tools for software development and
modern design solutions will bring several improvements and
simplifications during this upgrade.
\end{abstract}

\section{Introduction}\label{sec:introduction}
Neutrinos are the perfect probe to explore the far Universe. They have
no electrical charge, are insensitive to magnetic fields and interact
only weakly. This means they can travel huge distances from their
production sites before reaching a detector on the Earth, transporting
direct information on mechanisms acting inside cosmic
accelerators. The discovery of astrophysical neutrinos will open a new
perspective of astronomy and astrophysics, complementing present gamma
ray astronomy and cosmic ray studies~\cite{rays}.

However, because of the same properties that make them unique
messengers of high-energy processes, there are severe limitations to
detectability of high-energy neutrinos. Very large detection volumes,
at least 1~km$^3$, are required in order to have an
unambiguous signal due to astrophysical neutrinos together with an
effective shield against the overwhelming background due to
atmospheric muons, residuals of the high-energy cosmic ray showers.

KM3NeT-Italy is an INFN project co-financed by the European Community
and the Italian Government. Its main objective is the construction of
a 1~km$^3$ high-energy neutrino detector telescope in
the Ionian Sea (Italy) that will also host an interdisciplinary
observatory for marine sciences~\cite{observatory}. In its final
phase, the detector will use about 5000 PMTs to measure the Cherenkov
light emitted by high-energy muons created in ultra high-energy
astrophysical neutrino interactions, whose signal-to-noise ratio is
quite disfavoured. This forces the use of an on-line triggering system
in order to reject as much background as possible.

In March 2013, a prototype Detection Unit (DU), following the tower
layout and hosting 32 PMTs, has been deployed in the abyssal site of
Capo Passero and successfully operated.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\textwidth]{nemo.jpg}
  \caption{\label{fig:tower}
    The prototype tower during the deployment.
  }
\end{figure}

Since then, the on-line Trigger and Data Acquisition System (TriDAS)
has been continuously running at the Portopalo control station~\cite{tridas-phase2}. At the
beginning of 2015, the detection volume will be expanded with 8 more
towers, each hosting 84 PMTs, completing the KM3NeT-Italy Phase 1
detector. KM3NeT-Italy is also the italian node of KM3NeT, a
second-generation distributed neutrino telescope~\cite{km3net}.

\section{The evolution of the TriDAS software}\label{sec:cnaf}

TriDAS, designed for the prototype tower deployed by KM3NeT-Italy and
running at the off-shore station of Portopalo, has been stressed with
a long period of data acquisition, started more than a year ago. After
an important upgrade of the dedicated computing infrastructure and a
revision of the existing software, TriDAS will also be used for the
data acquisition of the first block of the KM3NeT telescope, that is
composed of 8 towers. Each tower follows the prototype layout but with
84 PMTs instead of 32.

In order to meet the new requirements, the revision of TriDAS has made
use of the services offered by the ISSS project and has involved the
adoption of modern software design solutions and high-level libraries.

ISSS (Infrastruttura per il Supporto allo Sviluppo Software) is an
INFN project led by CNAF~\cite{isss1,isss2,servnaz}, whose primary goal is
the creation of an infrastructure offering a variety of tools and
services already configured to support software development for
projects inside INFN.

\subsection{C++ Boost libraries}

Boost~\cite{boost} is a set of open-source C++ libraries that extend
the functionality of the C++ standard library. Many of Boost's
founders are themselves in the standards committee and several Boost
libraries have been accepted for incorporation into the C++11
standard.

The use of Boost libraries in TriDAS, in addition to increasing the
efficiency and maintainability of code, facilitates the transition to
future C++ standards.

\subsection{ZeroMQ}

ZeroMQ~\cite{zmq} (also known as zmq) is both an embeddable networking
library and a concurrency framework. It provides sockets that carry
atomic messages across various transports like in-process,
inter-process, TCP, and multicast. It allows to connect sockets N-to-N
with patterns like fan-out, pub-sub, task distribution and
request-reply. It has an asynchronous I/O mode, APIs for the main
languages and runs on most operating systems. ZeroMQ has been chosen
to implement communication and monitoring functionality in TriDAS.

\subsection{CMake}

CMake~\cite{cmake} is cross-platform open-source software for managing
the build process of software, using a compiler-independent method. It
is designed to simplify the compilaton process: distinguished by a
modular structure, it provides an easy way to create dedicated
Makefiles.

The introduction of CMake in TriDAS has simplified the steps of
compilation, testing, packaging and deployment, allowing the execution
of unit and integration tests after compilation and the creation of
self-installing scripts and archives.

\subsection{Jenkins}

Jenkins~\cite{jenkins} is an open-source continuous integration tool
that provides continuous integration services for software
development. Builds can be started by various means, including being
triggered by a commit in a version control system, scheduling via a
cron-like mechanism, building when other builds have completed, or by
accessing a specific build URL.

A Jenkins service, together with a pool of nodes with different
operating systems and architectures, is available through the ISSS
project: this service is used to compile and test TriDAS every night
automatically on different platforms.

\subsection{git and Bitbucket}

{\em git}~\cite{git} is an open-source distributed version control
system, characterized by a strong support for non-linear development,
making it ideal for projects that require an extensive collaboration.

For the development of TriDAS git has been chosen instead of SVN,
using Bitbucket~\cite{bitbucket} as a web-based service hosting the
source code. Bitbucket offers additional services, notably in support
of code review, which have been extensively used during development.

\subsection{JIRA}

JIRA~\cite{jira} is a project management system that lets users
prioritize, assign, track, report and audit issues, from software bugs
and helpdesk tickets to project tasks and change requests. JIRA is
also an extensible fully-customizable platform. A TriDAS project is
hosted on the INFN JIRA service, which keeps track of the evolution of
the code.

\section*{References}

\begin{thebibliography}{99}
\bibitem{rays} T. Chiarusi and M. Spurio, 2010, High-Energy
  Astrophysics with Neutrino Telescope, European Physics Journal C 65,
  nn. 3-4, 649
\bibitem{observatory} A. Margiotta, The KM3NeT project: status and
  perspectives, Geosci. Instrum. Method. Data Syst., 2, 35-40,
  doi:10.5194/gi-2-35-2013, 2013
\bibitem{tridas-phase2} C. Pellegrino et al, The Trigger and Data
  Acquisition for the NEMO-Phase 2 Tower, Proceedings for the VLVNT
  2013, Stockholm, in press
\bibitem{km3net} A. Margiotta, The KM3NeT detector, this report
\bibitem{isss1} S. Antonelli et al., An integrated infrastructure in
  support of software development, Proceedings of CHEP 2013,
  J. Phys. Conf. Ser. 513, 2014
\bibitem{isss2} S. Antonelli et al., Software development made
  easier, this report
\bibitem{servnaz} S. Antonelli et al., National ICT infrastructures
  and services, this report
\bibitem{boost} Boost C++ Libraries \url{http://www.boost.org/}
\bibitem{zmq} ZeroMQ \url{http://zeromq.org/}
\bibitem{cmake} CMake \url{http://www.cmake.org/}
\bibitem{jenkins} Jenkins \url{https://jenkins-ci.org/}
\bibitem{git} git \url{http://www.git-scm.com/}
\bibitem{bitbucket} Bitbucket \url{https://bitbucket.org/}
\bibitem{jira} Atlassian JIRA
  \url{https://www.atlassian.com/software/jira}
\end{thebibliography}

\end{document}
