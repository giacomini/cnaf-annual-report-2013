\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\newcommand{\numuTOnue}{$\nu_{\mu} \rightarrow \nu_e$ }
\newcommand{\numuTOnutau}{$\nu_{\mu} \rightarrow \nu_{\tau}$ }
\newcommand{\numu}{$\nu_{\mu}$ }
\newcommand{\nue}{$\nu_e$ }

\begin{document}
\title{The ICARUS Experiment}

\author{M.~Antonello on behalf of the ICARUS Collaboration}

\address{INFN - Laboratori Nazionali del Gran Sasso, Assergi, Italy}

\ead{maddalena.antonello@lngs.infn.it}

%\begin{abstract}
%Abstract...
%\end{abstract}

\section{ICARUS experiment scientific program}
In 1977 C.Rubbia~\cite{Carlo_Rubbia} conceived the idea of the LAr-TPC (Liquid Argon Time Projection Chamber). 
The ICARUS T600 cryogenic detector is the largest LAr-TPC ever built and operated. Installed in the Hall B of the Gran Sasso underground laboratory and exposed to the CNGS neutrino beam, on June 26$^{th}$ 2013 ICARUS has completed 3 years of continuous data taking, collecting about 3000 CNGS neutrino events but also cosmic rays and other self triggered events, and showing optimal overall detector performance and stability. 
The ICARUS T600 addresses a wide physics program including atmospheric and solar neutrino interactions and also charged and neutral current neutrino interactions associated with the CNGS neutrino beam, focusing on neutrino oscillation studies in the \numuTOnue, \numuTOnutau and \numu disappearance channels. It can also search for rare and up to now unobserved events like the long sought for proton decay with zero background in one of its 3$\times$10$^{32}$ nucleons (in particular into exotic channels).

\subsection{Experimental results}
Recently, ICARUS has published updated results on the search for anomalous "LSND-like" \numuTOnue transitions~\cite{nueupdate,nue}. The outcome allowed to strongly limit the window of open options for the LSND anomaly in a well defined parameter region centered around $(\Delta m^2, sin^2 (2\theta)) = (0.5eV^2, 0.005)$ where there is an over-all agreement (90\% CL) between the present ICARUS limit, the published limits
of KARMEN~\cite{karmen} and the published positive signals of LSND~\cite{lsnd} and MiniBooNE~\cite{miniboone}
collaborations.\\
%The tension between our limit and the neutrino lowest energy points of MiniBooNE ( $200 < E_{\nu} < 475$ MeV), suggests an instrumental or otherwise unexplained nature of the low energy signal reported by MiniBooNE. 
%The present result is based on the analysis of a sample of 6$\cdot$10$^{19}$ pot of the CNGS beam\footnote{An additional sample corresponding to ~1$\cdot$10$^{19}$ pot is being analyzed and will be released soon.}.
In the last years, ICARUS T600 also gave a prompt contribution in the rejection of the so called "superluminal neutrino" hypothesis through a search for the analogue to Cherenkov radiation in the CNGS neutrino events~\cite{superluminal}, as predicted by Cohen and Glashow for superluminal neutrinos, and performing a precision measurement of the neutrino velocity using the CNGS bunched neutrino beam~\cite{precision_nutof,nutof}.\\
The successful operation of the ICARUS T600 experiment at LNGS has conclusively demonstrated that the LAr-TPC is the leading technology for the future short and long baseline accelerator-driven neutrino physics. A technical paper on the cryogenic system performance is under submission to JINST and a paper on the trigger system performance is at final stage of preparation.

\section{ICARUS T600 detector}
The ICARUS T600 detector consists of a large cryostat split into two identical, adjacent half-modules with internal dimensions 3.6 $\times$ 3.9 $\times$ 19.6 m$^3$ and filled with
a total of 760 tons of ultra-pure LAr. Each half-module houses two TPCs separated by a
common cathode, with a drift length of 1.5 m. Ionization electrons, produced by charged 
particles along their path, are drifted under uniform electric field (E$_D$ = 500 V/cm)
towards the TPC anode, made of three parallel wire planes, facing the LAr active volume. A total of about 54000 wires are deployed, with a 3 mm pitch, oriented on each plane at different angles with respect to the horizontal direction (0$^0$, + 60$^0$, - 60$^0$). The drift time of each ionization charge signal, combined with the electron drift velocity information (v$_D$ = 1.55
mm/s), provides the position of the track along the drift coordinate. Combining the wire
coordinate on each plane at a given drift time, a three-dimensional image of the ionizing event can be reconstructed~\cite{3Dpaper}. 

\subsection{Computing resources}
The ICARUS collaboration counts many INFN sections (LNGS, Milano, Padova, Pavia, Naples) but also a variety of foreign institution, mainly from Poland. The computing resources of the experiment have thus been dimensioned and organized in order to guarantee an easy and safe data accessibility from all the collaboration sites. 
An "on-line" storage system is located in the underground ICARUS control room. It hosts a 40 TB storage element, upgraded with an additional 50 TB element during the 2012 beam stop. The DAQ dedicated disks belong to a SAN (over a fiber channel network) and are directly mounted on every online machine, including the host installed in the external LNGS computing centre. 
The "off-line" local resources at the outside LNGS buildings count four 24TB raid-5 storage units, for a total of 72 TB, available online on a cluster of 6 processing units (8 CPU cores each). One of the processing units serves as access point to the Grid and installs the User Interface package. The offline system is completed by a 24-slot LTO4 Tape Library.
As additional resources for data analysis, one processing unit and one storage unit of the same type are replicated at the INFN sections of Milano, Padova, Pavia and Naples.
The ICARUS experiment is represented, on the Italian Grid Infrastructure, by a Virtual Organization named {\it icarus-exp.org}. A buffer of about 150 TB is reserved online for ICARUS at the Tier1, where a D0T1 sevice is provided. Wide band, easy and safe accessibility of data is guaranteed for all the groups in the ICARUS collaboration involved in the analysis.

\subsection{Data management}
An ICARUS event is defined as the whole of the information coming from the two T600 half-modules: the event size is of about 100 MB and the global average trigger rate\footnote{The global rate includes the cosmic event trigger, the CNGS neutrino trigger and trigger based on local charge deposition inside the detector.} is about 350 mHz, driving to a total throughput of 87 TB per month. A big effort has been taken in order to reduce the total throughput to a much lower value.
At each trigger, performed exploiting both the LAr scintillation properties and the local ionization charge deposition inside the detector, one waveform per wire (i.e. per electronic channel) is collected; waveforms are packed in four different streams, one per TPC.
Data are initially stored on the online storage system in the underground control room. The ICARUS data flow has been structured in five sequencial steps: (1) Data Quality Monitor; (2) data streams merging and zipping; (3) empty events filtering through the Cosmic Event software Filter (CEF); (4) data transfer to the offline storage system; (5) long term data storage on tape.
The Data Quality Monitor checks the compatibility, in time, of the four data streams tagged with the same event number by the DAQ. If the event passes this first filter, the four data streams are copied and contextually merged and zipped with a lossless compression factor of about 56\%. Original streams are preserved for redundancy: from this stage on, a double copy of each event is guaranteed by the data-flow configuration, up to the final destination of data.
After an initial validation phase, during which all events where anyway preserved up to the final stage of the data flow, the CEF was finally applied for an effective data reduction: events classified as empty by the CEF were deleted\footnote{One empty event every thirty was anyway kept as checking sample.}, with a reduction to about the 15\% of the total amount of data. After empty events filtering, the global trigger rate gets to about 50 mHz, being dominated by cosmic ray events. Considering the lossless data compression,  the total data throughput is of 7 TB per month.
The data transfer from the online to the offline ICARUS storage system were performed through the 8 km optical fiber connecting the underground Hall B and the LNGS outside buildings. 
Data were continuously transferred from the offline storage to the tape filesystem facility at CNAF Tier1, at a maximum rate of 30 MB/s. In order to ensure a proper level of redundancy, data were also copied on LTO4 tapes at LNGS. 
The final double copy on tape of the ICARUS data is the green light for data deletion from the online and the offline storage buffers, thus freeing disk space for further data taking. In this sense, the continuous availability of CNAF sevices, including the technical support to the collaboration, can be regarded as an important ingredient of a continuos and successful data taking.

\section*{References}
\begin{thebibliography}{9}
\bibitem{Carlo_Rubbia} C.~Rubbia, CERN-EP/77-08 (1977).
\bibitem{nueupdate} M.~Antonello et al. [ICARUS Coll.], Search for anomalies in the \nue appearance from a \numu beam , Eur.Phys.J. C73 (2013) 2599.
\bibitem{nue} M.~Antonello et al. [ICARUS Coll.], Experimental search for the LSND anomaly with the ICARUS LAr TPC detector in the CNGS beam, Eur. Phys. J. C, 73:2345 (2013).
\bibitem{karmen} B. Armbruster et al. (KARMEN), 
%"Upper limits for neutrino oscillations $\bar\nu_\mu\to\bar\nu_e$ from muon decay at rest",
Phys. Rev. D 65 (2002) 112001, hep-ex/0203021.
\bibitem{lsnd} A.Aguilar et al. (LSND), Phys. Rev. D 64, 112007 (2001).
\bibitem{miniboone} A.Aguilar et al. (MiniBooNE), Phys. Rev. Lett. 110, 161801 (2013).
\bibitem{superluminal} M.~Antonello et al. [ICARUS Coll.], A search for the analogue to Cherenkov radiation by high energy neutrinos at superluminal speeds in ICARUS, Physics Letters B 711 (2012) 270-275.
\bibitem{precision_nutof} M.~Antonello et al. [ICARUS Coll.], Precision measurement of the neutrino velocity with the ICARUS detector  in the CNGS beam, Journal of High Energy Physics, JHEP 11 (2012) 049.
\bibitem{nutof} M.~Antonello et al. [ICARUS Coll.], Measurement of the neutrino velocity with the ICARUS detector at the CNGS beam, Physics Letters B 713 (2012) 17-22.
\bibitem{3Dpaper} M.~Antonello et al. [ICARUS Coll.], Precise 3D track reconstruction algorithm for the ICARUS T600 liquid argon time projection chamber detector, Adv. High Energy Phys. (2013)  260820.
\end{thebibliography}

\end{document}


