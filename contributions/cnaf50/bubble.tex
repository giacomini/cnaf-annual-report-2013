\documentclass[a4paper]{jpconf}

\begin{document}
\title{Bubble chambers and the \textit{Flying Spot Digitizer}}

\vspace{1cm}

The acronym CNAF, which stands for Centro Nazionale Analisi Foto-
grammi (National Center for Photographic Analysis), may sound quite
strange to those who know the Center for its current research activity
and who may wonder what photography has to do with networks or
distributed computing. The truth is that CNAF had to pass through some
radical transformations in order to keep up with the evolution of
nuclear and particle physics.

The origin of the name and the reasons why the Center was founded can
be understood only if we take a look at what was happening in nuclear
physics during the fifties. The invention of bubble chambers by Donald
A. Glaser in 1952 literally revolutionized the way nuclear and
particle physics research was performed.  Compared to nuclear
emulsion, the most precise and compact tracking detector for charged
particles used in those days, bubble chambers allowed to create 3D
spatial images of elementary particle interactions that were impressed
on photographic films at a relatively higher rate. As a consequence,
the production of data increased remarkably.

Bubble chambers started to be built in many nuclear physics labs and
soon grew in number and size. In Italy the interest for such a
promising technology became immediately apparent: it was only 1953
when the founding members of the future ”Bubble Chamber Group”, led by
Marcello Conversi, started their experimental activity in Pisa. Soon,
relatively large quantities of films were produced and then analyzed
in a few sites of the National Institute of Nuclear Physics (INFN)
where suitable measuring “projectors” were used to display the
magnified images. With the help of these devices, trained technicians
were able to locate many points on the tracks. This information used
to be converted into digital form and then collected and transferred
to obtain the geometrical track parameters and the kinematic
reconstruction of the events.

In the late fifties, the urgent need for more precise and faster
measurements was finally addressed by an international cooperation
effort (including CERN, DESY, Saclay, Brookhaven, Berkeley and several
other universities) whose goal was to build an automatic measurement
system of the bubble chamber pictures with precision of one micron.

Giampietro Puppi, director of the INFN unit based in Bologna, saw in
the local university environment the ideal conditions to set up a new
center with the specific mission of providing support for automatic
film measurements and for data processing, paving the way to the
creation of CNAF by INFN in 1962. The cooperation with the Bologna
unit of CNEN (National Committee for Nuclear Energy) was crucial both
for providing access to one of the most advanced computers of those
times (the IBM 7094), and for hosting the activities of the new
center.

Following an idea originally proposed by P.V.C. Hough and B.W. Powell,
the newly formed CNAF team designed and built a Flying Spot Digitizer
(FSD). The FSD scanned with a luminous spot and explored sequentially
every single film coming from the bubble chambers. This small spot of
light was projected by a lamp and shifted mechanically backwards and
forwards across each image in search for particle tracks. The tracks
modulated the light signal which passed through the film before it got
detected by photomultipliers. This procedure generated information on
track positions and made them available in electronic form (digital
coordinates) to a computer.

Since all films contained many tracks but just a few of them were
associated with significant particle interactions, it was necessary to
filter the data so that the noise and all irrelevant information could
be removed. This operation was implemented through
``pre-digitization'', a manual procedure which was carried out with
film projectors. The completion of the project owed most of its
success to the hard work and passion of the first Technical Director
Massimo Masetti and his group of about twelve collaborators. The
project started in 1962, whereas the entire system FSD-7094 was
operational in the early months of 1966 and inaugurated in the
presence of the University Minister Luigi Gui the following year.

Many were the original electronic, mechanical and optical components
that had to be developed in cooperation with specialized companies. A
ferromagnetic “thin film memory” was built in collaboration with the
company Olivetti to achieve read/write access time performance
suitable for the data acquisition peaks of the FSD. Thanks to this
innovation, it was possible to overcome the limitations of the IBM
7094 standard magnetic memory.

In 1968 CNAF bought an IBM 360/44, a new fast and efficient computer
for the on-line acquisition of the data produced by the FSD. CNAF
built an original advanced electronic interface to interconnect the
new IBM 360/44 to multiple external devices like a new FSD and some
new analyzer devices. Some INFN groups based in Bologna, Padua and
Trieste started to bring their films to CNAF, and the Center began to
play a major coordinating role for precise measurements and for
central computing. The FSD was made available to all Italian INFN
sites, and CNAF was in charge of coordinating its use by all different
groups.

To face the increasing computing demand from INFN sites, a new FSD was
made operational in 1971, and it reached the remarkable amount of
300.000 measured events per year. The bubble chamber pictures were
becoming more and more complex, and that implied an increase of the
noise produced; it was necessary to reduce the amount of data. A new
special purpose processor was designed and implemented to run online,
and it was able to transform a cloud of points in segments identified
by coordinates and slope. This processor, built with the most advanced
technologies of the time, was programmed in firmware.

In the meantime (ca. 1968) Proteus, another analyzing system device
whose features were similar to the FSD, was built in about one
year. The FSD opto-mechanical core device was replaced by a
high-resolution Ferranti cathode ray tube, and the scanning spot was
obtained by focusing the light produced by an electron beam which hit
a fluorescent screen.

Compared to the FSD, Proteus was less precise and had a lower
resolution, but it was more versatile because the electron beam could
be deflected as required by applying an electromagnetic field. Proteus
allowed to select the area to scan and so it decreased the
computational effort necessary for the noise reduction.  This device
was particularly efficient in the analysis of spark chamber events
whose topology was simpler than bubble chambers’.

The measuring process was sped up to such an extent that it was
possible to analyze up to 240 pictures per hour, i.e 100.000 events in
45 days. As a clear indication of both the versatility of the device
and of the openness of the Center to cooperate with other research
institutes, it is worth mentioning a paper produced in partnership
with the Department of Human Anatomy of the University of
Modena. Proteus was used for scanning photographic images of human
chromosomes in order to study their banding patterns.

The last development in the study of bubble chamber events was ERA-
SME, a CRT device far more complex and performing than Proteus. The
project started in 1974, and was intended for the analysis of films
produced by CERN with the big bubble chamber called BEBC. Based on the
measurement system built in Geneva, ERASME followed the steps of
pre-digitization and of automatic measurement. This effort was made
possible by a new advanced graphic and interactive system developed at
CNAF which allowed to overlap analog and digital information. The
expertise and know-how gathered by CNAF physicists and engineers in
inventing and managing digitizing devices was quite peculiar. Beside
the obvious competences in mechanics and optics, great efforts were
devoted to programming and optimizing data carrying connections. These
are the words used by Albert Werbrouk, first Scientific Director of
the Center, to recollect those achievements:

\begin{quote}

``Some programs bore names like Mist, Fog or Haze, names which summon
  the climate of the San Francisco Bay. The Hough-Powell device was
  initially developed at Berkeley, and we kept those names as a
  tribute. Other programs whose names were evocative of their
  functions, such as Thresh and Grind, came from CERN.  We had to
  write some original code to let different tools interact, as in the
  case of BIND, which converted data from the card-reader to a
  magnetic tape''

\end{quote}

The names used for those programs witness the heterogeneity of their
origins.  This heterogeneity forced CNAF computing professionals to
get a deep and well-grounded knowledge of different software, ranging
from programs developed by researchers to the managing systems of
commercial machines such as IBM computers. People at CNAF had to learn
to work efficiently and to adapt quickly to different platforms. At
the same time, it was necessary to cautiously manage the flux of data
from FSDs to computers and from computers to recorders or printers. A
few years later, the sound expertise in data transmission and storage
acquired by CNAF technicians in those years would be of great help
when bubble chambers had to be replaced by more effective electronic
instruments and the Center was to reconsider its mission and role.

\end{document}
