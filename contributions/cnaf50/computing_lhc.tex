\documentclass[a4paper]{jpconf}

\begin{document}
\title{The computing infrastructure of LHC}

\vspace{1cm}

In 1998, while CNAF started shifting its research mission, INFN was forced to
rethink its policy regarding the management of its computing resources.
Until that moment, the bulk of computing resources owned by INFN had been
limited to small computational farms for local use, while mainframes owned by
High Energy Physics Laboratories worldwide and other Italian centers such as
CINECA, CILEA, CASPUR had been employed by the Institute for bulk computation. However, the decision of participating in the Large Hadron Collider (LHC)
experiments in Geneva was going to change this situation completely. It was
soon realized that, with its expected 15 PB of data generated each year, LHC
would have represented a completely different challenge in terms of data analysis management.

As Laura Perini, a member of the ATLAS experiment, recalls:

\begin{quote}

``We realized that maintaining all the computational facilities in
  Geneva as had occurred for LEP was no longer possible. Contributors
  definitely preferred having direct control on the infrastructures
  they paid for, and they would have liked them to be in their own
  country. However, thanks to data transmission connections, the
  situation was different for computing centers. Our idea was to allow
  every single country involved in the LHC project to develop its own
  human and computing resources in loco, in order to spread skills and
  expertise all over Europe.''

\end{quote}

A research project named Monarc (Models of Networked Analysis at
Regional Centers) was launched. CNAF was involved in the project from
the beginning and gave a significant contribution to it. The resulting
proposal was a hierarchical architecture with computing resources to
be organized in three levels (TIERs).  The data produced by the LHC
accelerator from CERN (TIER-0) would have been transferred to the main
centers (TIER-1), located in some European countries, in the US and in
Asia, and responsible for data management and processing at a regional
scale. Secondary centers (TIER-2) would have referred to TIER-1
centers in order to get the data they were interested in, with no
direct connection to TIER-0.

This simple and rigid structure was justified by a precautionary
approach, since at that time networks were considered a possible
limiting factor for the expected huge amount of data to be managed on
a worldwide scale.

The introduction of a new, geographically distributed approach was ac
companied by other important changes: the object-oriented programming
paradigm was gaining consensus within the big HEP collaborations, and
the combination of commodity servers employing the Personal Computer
architecture and the open Linux operating system gradually became the
platform of choice for building up large scientific clusters.

INFN therefore started a big campaign with the aim of training
physicists and computer scientists to deal with new computing
technologies. CNAF, which took part in the newly established INFN CNTC
committee (Comitato Nuove Tecnologie di Calcolo --- New Computing
Technologies Committee), decided to organize courses to teach
programming languages like C++, Unix-based operating systems and the
TCP-IP protocol suite. Meanwhile, the INFN computing infrastructure
for LHC started to take form, and CNAF was soon identified as the
hosting site for the Italian TIER-1 because of its expertise in
running networks infrastructures, in providing computing national
services and for the experience gained in managing the INFNet and GARR
projects. In 2001 the project for the INFN TIER-1 at CNAF entered its
operational stage with the design of a prototype installation. Unlike
other sites across Europe, CNAF could not count on pre-existing
hardware, software and technical infrastructures suitable for a TIER-1
center.

Circumstances were challenging because a large and performant
computing center had to be to set up from scratch in a relatively
short time. The location for the facility was sought within the
premises of the Physics Department of the Bologna University, which
was already hosting the CNAF center. This choice allowed a constant
and close interaction between the two communities, but it inevitably
posed a few logistical issues, since no suitable place was immediately
available. In the end, a 1000 sqm garage floor in the basement of the
main building was identified for the purpose, and the design of the
computing rooms had to face several environmental constraints. At the
end of 2005, the TIER-1 prototype was fully functioning, and its
computing facilities consisted of about 1000 bi-processor servers
mounted on 35 racks, 450 TB of disk storage and a magnetic tape
library of 500 TB, managed by fully automated software procedures for
installation and configuration. The local area network consisted of 35
switches to aggregate the servers, and two core switches connecting
the center to the GARR network at 2Gbps.

The experience obtained with the prototype was very useful for
training the personnel and for identifying the most critical aspects
to be taken into account for delivering the services with the very
high (99,9\%) availability and reliability levels required by the LHC
experiments. It was realized that in order to achieve such goals,
complete redundancy of the electrical and HVAC systems was necessary
to avoid single points of failures and perform maintenance operations
with no impact on the running systems. The storage system was
identified as the most critical computing service: the first solution
adopted at CNAF could hardly comply with the LHC requirements. In this
respect, several campaigns of tests were launched to investigate
alternative solutions. The results of these tests led to the adoption
of a solution based on industrial standards: a parallel file-system
meant to ensure high availability and performances, and an
infrastructure based on Storage Area Network to allow flexibility and
easiness of management. The batch system responsible for managing the
delivery of computing jobs to the servers was also replaced by a more
mature and scalable solution, and, finally, the unification of all
computers into a single cluster made it possible to avoid any static
partitioning.

In 2005, a substantial infrastructure upgrade including building
refurbishment, new racks arrangement for the hot aisle containment and
a complete electrical and HVAC systems redesign was planned, and a
series of European tenders was announced. Improvement activities
started in 2008 and were completed after only nine months, as in March
2009 the Center attained its expected computing, storage and technical
configuration just in time for the start of data acquisition at
LHC. With 3.8~MW of available electrical power (3.5~MW in absolute
continuity), and six independent cooling plants capable of extracting
up to 1.5~MW of released heat, the computing room upgrade was designed
to exceed the foreseeable needs of the LHC experiments. At the
beginning of 2013 the Italian Tier1 was hosting more than 1300 servers
with 13,000 computing cores, 13 PByte of disk storage and a magnetic
tape library with 14 PByte of data. Network connections to the
external world were provided by GARR through three separate 10 Gbps
links. About 20 international scientific experiments were supported at
the Center; on average, 90,000 batch jobs were being executed every
day, with computing resources optimally used 24x7 by all experiments
thanks to sophisticated sharing policies applied to a single large
computing cluster capable of supporting local, Grid and Cloud resource
requests on both real and virtual environments.

\end{document}
