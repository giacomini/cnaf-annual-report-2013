\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}
\title{The ALICE experiment activities at CNAF}

\author{S. Bagnasco$^1$, D. Elia$^2$}

\address{$^1$INFN Torino, $^2$INFN Bari}

\ead{Stefano.Bagnasco@to.infn.it}


\section{Experimental apparatus and physics goal}

ALICE (A Large Ion Collider Experiment) is a general-purpose heavy-ion experiment specifically designed to study the physics of strongly interacting matter and QGP (Quark-Gluon Plasma) in nucleus-nucleus collisions at the CERN LHC (Large Hadron Collider). 

The experimental apparatus is composed of a central barrel detector, which measures event-by-event hadrons, electrons and photons, and a forward spectrometer to measure muons. It has been designed to cope with the highest particle multiplicities theoretically anticipated for \mbox{Pb--Pb} reactions and has been operational since the start-up of the LHC in 2009. The central part, embedded in a large solenoidal 0.5 T field magnet, covers polar angles from $45^{\circ}$ to $135^{\circ}$ over the full azimuth. It consists of a Inner Tracking System (ITS) of high-resolution silicon detectors, a cylindrical Time Projection Chamber (TPC), three particle identification arrays of Time-of-Flight (TOF), Ring Imaging Cherenkov (HMPID) and Transition Radiation (TRD) detectors, a high-resolution electromagnetic calorimeter (PHOS) and a large Pb-scintillator sampling calorimeter designed also for jet studies (EMCAL). The forward muon arm (covering polar angles from $2^{\circ}$ to $9^{\circ}$) consists of a complex arrangement of absorbers, a large 0.7 T field dipole magnet, and fourteen planes of tracking and triggering chambers. Several smaller detectors (ZDC, PMD, FMD, T0, V0) for global event characterization and triggering are located at small angles. An array of scintillators (ACORDE) on top of the large solenoidal magnet is used to trigger on cosmic rays. An extension (DCAL) of the EMCAL, a second arm complementing EMCAL at the opposite azimuth and thus enhancing the jet and di-jet physics, is being installed during the Long Shutdown 1 period of LHC. A detailed description of the ALICE sub-detectors can be found in \cite{1}.

The main goal of ALICE is the study of the hot and dense matter created in ultra-relativistic nuclear collisions. At high temperature the Quantum CromoDynamics (QCD) predicts a phase transition between hadronic matter, where quarks and gluons are confined inside hadrons, and a deconfined state of matter known as Quark-Gluon Plasma \cite{2,3}. Such deconfined state was also created in the primordial matter, a few microseconds after the Big Bang. The ALICE experiment creates the QGP in the laboratory through head-on collisions of heavy nuclei at the unprecedented energies of the LHC. The larger the colliding nuclei and the higher the centre-of-mass energy, the greater the chance of creating the QGP: for this reason, ALICE has also chosen lead, which is one of the largest nuclei readily available. In addition to the \mbox{Pb--Pb} collisions, the ALICE Collaboration is currently studying pp and \mbox{p--Pb} systems, which are also used as reference data for the nucleus-nucleus collisions.

\section{Main physics results}

After the first three years of operations the ALICE has obtained important results studying in detail the hot matter produced in \mbox{Pb--Pb} collisions, with particular emphasis on correlations, heavy flavour and particle production. These results impose more stringent constraints for the various QCD models describing the QGP and the hadronization phase. The physics results of ALICE also include several measurements in proton-proton and proton-nucleus collisions, as well as photoproduction using ultra-peripheral collisions in \mbox{Pb--Pb}.

The experiment has collected data on \mbox{Pb--Pb} collisions at 2.76 TeV per nucleon pair in 2010 and 2011, pp collisions up to 8 TeV from 2009 to 2012 and \mbox{p--Pb} collisions at 5.02 TeV in 2012 and 2013. The measurements with proton beams, which are the baseline for the study of the heavy-ion collisions, have also produced genuine physics results: among them, the very first measurements of the charged-particle multiplicity and rapidity density at the LHC \cite{4,5} provided the basic ingredients for the tuning of the Monte Carlo generators. The first measurements with \mbox{Pb--Pb} collisions provided results on the charged-particle multiplicity (found to be around 1600, well below most of the extrapolations from RHIC) \cite{6}, the size and life time of the system created in the collision (increased by factors of 2 and 1.4 with respect to RHIC, respectively) \cite{7} and the elliptic flow coefficient v$_2$ (increased by about 30\% with respect to RHIC and in agreement with the hydrodynamic description of a strongly interacting very low viscosity fluid) \cite{8}.

Along the last years many other results have provided detailed information on the system created in \mbox{Pb--Pb} collisions at the LHC and how such system is described by the various hydrodynamic and thermal models. On the heavy flavour sector, results on the production of $J/\psi$, $\Upsilon$ and $D$ mesons (nuclear modification factors $R_{AA}$, flow, particle ratios etc) \cite{9,10,11} have cast new light on the interaction of hard probes with the medium, providing the first indications of a mass dependence of the energy loss in the QGP. In the light flavour sector, the spectra and correlations of identified particles have been published both for \mbox{Pb--Pb} and \mbox{p--Pb} collisions \cite{12}\cite{13}. In particular, the latest data collected in 2013 on \mbox{p--Pb} collisions have allowed the evaluation of cold nuclear effects in the long rapidity range correlations (ridge effect) \cite{14} and are currently being compared with different calculations including shadowing, saturation and hydrodynamical expansion, in order to elucidate the origin of such effects. Finally, results on $J/\psi$ production in ultra-peripheral \mbox{Pb--Pb} collisions have been obtained for the first time at the LHC \cite{15}, allowing comparison with data from HERA and providing discrimination with respect to QCD models and new input on nuclear particle distribution functions.


\section{Computing model}

The ALICE computing model is heavily based on Grid distributed computing; since the very beginning, the base principle underlying it has been that every physicist should have equal access to the data and computing resources \cite{16}. According to this principle, the ALICE peculiarity has always been to operate its Grid as a cloud of computing resources (both CPU and storage) with no specific role assigned to any given centre, the only difference between them being the Tier to which they belong. All resources are to be made available to all ALICE members, according only to experiment policy and not on resource physical location, and data is distributed according to network topology and availability of resources and not in pre-defined datasets. 

Thus, Tier-1s only peculiarities are their size and the availability of tape custodial storage, which holds a collective second copy of raw data and allows the collaboration to run reconstruction passes there. In the ALICE model, though, tape recall is almost never done: all useful data reside on disk, and the custodial tape copy is used only for safekeeping. All data access is done through the xrootd protocol, either through the use of “native” xrootd storage or, like in many large deployments, using xrootd servers in front of a distributed parallel filesystem like GPFS.

In the original MONARC architecture, the Grid had a hierarchical layout, with large Tier-1 centres acting as a pivot for a number of smaller Tier-2 centres in the same region; with the partially unforeseen availability of reliable high bandwith network connections between centres, at a relatively cheap price, the hierarchy is becoming more and more blurred. At the same time, alongside the Grid centres providing batch-like computing power, a number of Interactive Analysis Facilities have been born based on PROOF, that complement the Grid centres providing rapid turn-around resources for analyses that require a smaller dataset or for tuning on smaller samples analyses that will then be run on full statistics on the Grid \cite{17}.

The Italian share to the ALICE distributed computing effort (currently about 20\%) includes resources both form the Tier-1 at CNAF and from the Tier-2s in Torino, Bari, Catania and Padova/Legnaro, plus some extra resources in Cagliari, Bologna and Trieste.

Since March 2013, the activity to define and develop the new computing framework for the post-Long Shutdown 2 phase has been officially started within the Collaboration. The corresponding project (O$^2$ Project), which is mainly based on the concepts of Online-Offline integration and Cloud computing, has been organized in several dedicated Computing Working Groups which are expected to provide a Technical Design Report by October 2014.


\section{Role and contribution of the INFN Tier-1 at CNAF}

CNAF is a full-fledged ALICE Tier-1 centre, having been one of the first to enter the production infrastructure years ago. According to the ALICE computing model, it has no special given task among Tier-1 centres, but provides computing and storage resources to all the collaboration, along with offering a valuable support staff for the experiment’s computing activities.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=28pc]{ALICE-active-jobs.png}
\end{center}
\caption{\label{fig:activejobs} Ranking of CNAF among ALICE Tier-1 centres in 2013}
\end{figure}

\begin{figure}[ht]
\begin{center}
\includegraphics[width=28pc]{ALICE-CPU-time.png}
\end{center}
\caption{\label{fig:cputime} Running jobs profile at CNAF in 2013}
\end{figure}

It provides reliable xrootd access both to its disk storage and to the tape infrastructure, though a TSM plugin that was developed by CNAF staff specifically for ALICE use.

The computing resources provided for ALICE at the CNAF Tier-1 centre were fully used during last year, matching and exceeding the pledged amounts by as much as 150\% when exploited resources unused by other collaborations (Figure \ref{fig:activejobs}). Overall, about 60\% of the activity was Montecarlo simulation, 10\% reconstruction (which takes place at the Tier-0 and Tier-1 centres only), 10\% organized analysis (the so-called “analysis trains) and 20\% chaotic end-user analysis.

In 2013, CNAF provided 7.9\% of the total CPU hours used by ALICE, thus ranking third of the ALICE sites (following Prague and FZK in Karlsruhe and excluding the Tier-0 at CERN), corresponding to about 60\% of the total INFN contribution: it successfully completed more that 6.6 million jobs, for a total of 21.9 millions CPU hours (Figure \ref{fig:cputime}).

ALICE keeps on disk at CNAF about 1.3 PB of data, plus about 700 TB of raw data on custodial tape storage; the reliability of the storage infrastructure is commendable, even taking into account the extra layer of complexity introduced by the xrootd interfaces. Also network connectivity has always been reliable; furthermore, the recent upgrade to 40Gb/s of the WAN link makes CNAF one of the better-connected sites in the ALICE Computing Grid.

\section{References}

\begin{thebibliography}{10}
\bibitem{1} K. Aamodt et al. (ALICE Collaboration), JINST {\bf 3} S08002 (2008).
\bibitem{2} N. Cabibbo, G. Parisi, Phys. Lett. {\bf B 59} 67 (1975).
\bibitem{3} E.V. Shuryak, Phys. Rep. {\bf 61} 71 (1980).
\bibitem{4} K. Aamodt et al. (ALICE Collaboration), Eur. Phys. J. {\bf C 65} 111-125 (2010).
\bibitem{5} K. Aamodt et al. (ALICE Collaboration), Eur. Phys. J. {\bf C 72} 2124 (2012).
\bibitem{6} K. Aamodt et al. (ALICE Collaboration), Phys. Rev. Lett. {\bf 105} 252301 (2010).
\bibitem{7} K. Aamodt et al. (ALICE Collaboration), Phys. Lett. {\bf B 696} 328-337 (2011).
\bibitem{8} K. Aamodt et al. (ALICE Collaboration), Phys. Rev. Lett. {\bf 105} 252302 (2010).
\bibitem{9} B. Abelev et al. (ALICE Collaboration), J. High Energy Phys. {\bf 9} 112 (2012).  
\bibitem{10} E. Abbas et al. (ALICE Collaboration), Phys. Rev. Lett. {\bf 111} 162301 (2013).  
\bibitem{11} B. Abelev et al. (ALICE Collaboration), Phys. Rev. Lett. {\bf 111} 102301 (2013).  
\bibitem{12} B. Abelev et al. (ALICE Collaboration), Phys. Rev. Lett. {\bf 109} 252301 (2012).  
\bibitem{13} B. Abelev et al. (ALICE Collaboration), Physics Letters {\bf B 728} 25-38 (2014).
\bibitem{14} B. Abelev et al. (ALICE Collaboration), Physics Letters {\bf B 726} 164-177 (2013).
\bibitem{15} B. Abelev et al. (ALICE Collaboration), Physics Letters {\bf B 718} 1273-1283 (2013).
\bibitem{16} P. Cortese et al. (ALICE Collaboration), CERN-LHCC-2005-018 (2005).
\bibitem{17} S. Bagnasco et al., Journal of Physics: Conf. Series {\bf 368} 012019 (2012).
\end{thebibliography}

\end{document}
$
