\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{url}

\begin{document}
\title{Software development made easier}

\author{
  S Antonelli$^1$,
  C Aiftimiei$^2$,
  M Bencivenni$^1$,
  C Bisegni$^3$,
  L Chiarelli$^4$,
  D De Girolamo$^1$,
  F Giacomini$^1$,
  S Longo$^1$,
  M Manzali$^1$,
  R Veraldi$^1$
  and S Zani$^1$
}

\address{$^1$ INFN-CNAF, Bologna, Italy}
\address{$^2$ INFN, Padova, Italy}
\address{$^3$ INFN-LNF, Frascati, Italy}
\address{$^4$ GARR, Roma, Italy}

\ead{
  stefano.antonelli@cnaf.infn.it,
  cristina.aiftimiei@pd.infn.it,
  marco.bencivenni@cnaf.infn.it,
  claudio.bisegni@lnf.infn.it,
  lorenzo.chiarelli@garr.it,
  donato.degirolamo@cnaf.infn.it,
  francesco.giacomini@cnaf.infn.it,
  stefano.longo@cnaf.infn.it,
  matteo.manzali@cnaf.infn.it,
  riccardo.veraldi@cnaf.infn.it,
  stefano.zani@cnaf.infn.it
}

\begin{abstract}
This paper describes an infrastructure that is being made available to
software developers within INFN to support and facilitate their daily
activity. The infrastructure aims at integrating several tools, each
providing a well-identified function: project management, version
control system, continuous integration, dynamic provisioning of
virtual machines, efficiency improvement, knowledge base. When
applicable, access to the services is based on the INFN-wide
Authentication and Authorization Infrastructure. The infrastructure
will be beneficial especially for small- and medium-size
collaborations, which often cannot afford the resources, in particular
in terms of know-how, needed to set up such services.
\end{abstract}

\section{Introduction}\label{sec:introduction}

The success of a scientific experiment depends, often significantly,
on the ability to collect and later process large amounts of data in
an efficient and effective way. Despite the enormous technological
progress in areas such as electronics, networking and storage, the
cost of the computing factor remains high. Moreover the limits reached
by some historical directions of hardware development, such as the
saturation of the CPU clock rate with the consequent strong shift
towards hardware parallelization, has made the role of software more
and more important.

The ISSS (Infrastruttura di Supporto allo Sviluppo Software,
Infrastructure in Support of Software Development) project~\cite{isss}
aims at presenting INFN researchers with a variety of tools already
configured to support established best practices, so that the quality
of the software they produce could be continuously improved at a
decreasing cost. The generic term {\em quality} refers to
characteristics such as low presence of defects, runtime performance
efficiency, maintainability, easy portability to new
platforms. Similarly, the generic term {\em cost} covers many aspects,
such as time devoted to development, test, support and maintenance,
money spent in hardware resources and electrical power, low service
reliability.

\section{Current status of the project}\label{sec:status}

This document summarizes the current status of the project, with a
focus on the end user-facing services that have reached a production
or pre-production status: project management, version control and
continuous integration functionality. Other functionality, notably the
virtualization infrastructure that represents the foundation on top of
which the abovementioned services rely, is described in another
contribution in this report. A more thorough introduction of the whole
project is presented elsewhere~\cite{chep}.

It is worth noting that, when applicable, access to the services is
based on the INFN-wide Authentication and Authorization Infrastructure
(AAI). For web applications the AAI offers a SAML-based Identity
Provider (IdP) that allows a user to authenticate via either a
personal X.509 certificate or a username/password pair. The AAI is
available also to non-INFN users via a simple registration procedure,
permitting also to projects not entirely formed by INFN users to
access this infrastructure.

\subsection{Project management}

Any software project that goes beyond a limited-duration single-person
exercise would benefit from a project management system. The more
complex the project is (more developers, more users, larger code
base), the larger the benefit. We have chosen Atlassian {\em
  JIRA}~\cite{jira}, a widely-used tool that offers excellent support
for large and complex development projects involving many users. It
covers many aspects of the software project lifecycle, including:
interaction with users for requirements and support; organization of
issues, tasks and activities; integration with code repositories;
reporting.

\subsection{Version control}

A {\em Version Control System} (VCS) allows to store permanently any change
applied to a code base (source code, tests, documentation, build and
packaging instructions, etc.) along with metadata to keep track of the
history of the changes.

{\em Subversion}~\cite{subversion} has been chosen as version control system
to cover existing needs. The authentication to the system is based on
SSH public keys. Beside Subversion, solutions for {\em git}~\cite{git} and
{\em mercurial}~\cite{mercurial} repositories hosted by third-parties, such
as {\em GitHub}~\cite{github} and {\em Bitbucket}~\cite{bitbucket}, are encouraged
and well supported by the other tools, notably JIRA and Jenkins.

\subsection{Continuous integration}

In order for the developers to keep their confidence high that changes
applied to the code base, by themselves or by their peers, do not
cause regressions, it is a recommended practice to verify continuously
(e.g. periodically or even at every change) that all changes
introduced in the software at least build correctly and pass basic
tests. As an additional benefit, the automatic build and test phases
are an excellent occasion to run other quality checks. Several static
and dynamic analysis tools exist that are able to expose actual or
potential defects in the code before they reach production.

\begin{figure}[ht]
  \centering
  \setlength\fboxsep{0pt}
  \setlength\fboxrule{0.5pt}
  \fbox{
    \includegraphics[width=0.5\textwidth]{swprocess2.png}
  }
  \caption{\label{fig:swprocess}
    A typical software process using a continuous integration
    approach.
  }
\end{figure}

{\em Jenkins}~\cite{jenkins} provides the framework for continuous
integration and represents the foundation for any process that aims at
producing high-quality software components in a reproducible way, as
shown for example in figure~\ref{fig:swprocess}. A few slaves are
attached to the system, covering the most common Linux distributions
for 32- and 64-bit platforms.

\section{Conclusions}\label{sec:conclusion}

The success of a scientific experiment relies more and more on sound
computing practices, notably in the development of scientific
software.

The ISSS project aims at providing a one-stop shop for software
developers, with a special focus on members of small- and medium-size
experiments, where they can find state-of-the-art tools and services
that help them deliver software of increasing quality at lower costs
and on time, through the adoption of established best practices.

\section*{References}

\begin{thebibliography}{9}
\bibitem{isss} The ISSS Project \url{https://web.infn.it/isss}
\bibitem{chep} Antonelli S et al 2014 An integrated infrastructure in support of software
development {\em J. Phys.: Conf. Ser. Proceedings of CHEP2013} {\bf 513} In Press 
\bibitem{jira} Atlassian JIRA
  \url{https://www.atlassian.com/software/jira}
\bibitem{jenkins} Jenkins \url{https://jenkins-ci.org/}
\bibitem{subversion} Subversion \url{https://subversion.apache.org/}
\bibitem{git} git \url{http://git-scm.com/}
\bibitem{mercurial} mercurial \url{http://mercurial.selenic.com/}
\bibitem{github} GitHub \url{https://github.com/}
\bibitem{bitbucket} Bitbucket \url{https://bitbucket.org/}
\end{thebibliography}

\end{document}
