%% LyX 2.0.2 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{jpconf}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\newcommand{\lyxaddress}[1]{
\par {\raggedright #1
\vspace{1.4em}
\noindent\par}
}

\makeatother

\usepackage{babel}
\begin{document}

\title{EMI Testbed Improvements and Lessons Learned from the EMI 3 Release}

\author{F Capannini$^1$, B Hagemeier$^2$, A Elwell$^3$, C Bernardt$^4$, M Kocan$^5$, F Dvorak$^6$,
D Dongiovanni$^1$, C Aiftimiei$^7$, A Ceccanti$^1$ and A Cristofori$^1$}

\address{$^1$ INFN-CNAF, Viale Berti Pichat 6/2, I-40127 Bologna Italy}
\address{$^2$ Forschungszentrum J�lich GmbH, 52428 J�lich, Germany}
\address{$^3$ CERN Geneve, Switzerland }
\address{$^4$ DESY Notkestra�e 85 D-22607 Hamburg, Germany }
\address{$^5$ Faculty of Sciences, UPJS Jasenna 5 040 01 KOSICE - Slovakia }
\address{$^6$ CESNET University of West Bohemia, Univerzitni 20, 306 14 Plzen,
Czech Republic }
\address{$^7$ INFN Padova, Via Marzolo 8, I-35131 Padova, Italy }

\ead{fabio.capannini@cnaf.infn.it}

\begin{abstract}
The European Middleware Initiative (EMI) Project has succeeded in
merging into a set of releases (EMI 1 Kebnekaise, EMI 2 Matterhorn)
more than fifty software products from four major European technology
providers (ARC, gLite, UNICORE and dCache). To satisfy end user expectation
in terms of functionality and performance, the release process implements
several steps of certification and verification. The final phases
of certification are aimed at harmonizing the strongly inter-dependent
products coming from various development teams through parallel certification
paths. This article introduces the new approach in the design and
management of the release process envisaged for the EMI 3 release
according to the requirement of a more effective integration strategy
emerged during the first two EMI releases.
\end{abstract}

\section{Role of central testing facilities as part of EMI release cycle and
quality assurance activities }

The evolution of EMI software products in order to fix software errors
or implement new features follows a defined release cycle, resulting
in both monthly release updates (minor releases whilst not breaking
backward compatibility) and yearly major releases. To these periodical
releases we add revision (fixing defeats without introducing new features)
and emergency updates (fixing problems with top priority, generally
related to security). The release process periodically cycles over
five macro phases:
\begin{enumerate}
\item phase 1-) Requirements analysis phase: inputs collected from EMI user
communities representatives are translated into accepted technical
requirements;
\item phase 2-) Development and test planning phase: technical requirements
from phase 1 bring to new development and test plans; 
\item phase 3-) Development, testing and certification phase: new products
versions are developed according to test plans and tested by product
teams; 
\item phase 4-) Release certification and validation phase of new products
candidates; 
\item phase 5-) Release and maintenance; 
\end{enumerate}
The EMI quality assurance work package is in charge of those activities
aimed at harmonizing the parallel work of the developer product teams
to obtain as output a single homogeneous EMI release. Therefore, among
quality assurance duties we have the definition and monitoring policies,
definition and collection of metrics and keys performance indicators
(KPIs), quality control verification and reporting, the provision
of common tools for products building and the implementation of common
and shared infrastructural and operational resources for product inter-component
and large scale testing. The present work focuses on this last working
area in EMI quality assurance, which is strictly related to the phase
4 and 5 of release cycle. Given the framework described above, we
can summarize the role of central testing infrastructure team as a
provider of all facilities and certification activities assuring that
the sum of components certified in isolation constitutes an EMI release
of products deployable from single repository and consistently inter-operating. 


\section{Overview of EMI Products certification testing }

Each EMI component follows an independent path from other components
life cycle during the definition of requirements, development/test
planning, source coding, build until the component certification in
isolation. Then, after certification in isolation has been accomplished,
the various component release paths must intersect in order to verify
that all components can consistently inter-operate. This means that
the logically unitary phase of component certification, aimed at verifying
the expected functioning under production environment, is actually
split in two separate steps across EMI release cycle phases 3 and
4. Moreover, component certification in isolation during phase 3 is
performed on product team resources while inter-component testing
performed during release phase 4 occurs on central testbed resources.
To give an overview of the types of tests performed in each phase
we mention: 
\begin{enumerate}
\item Release phase 3: static code analysis, installation tests during repackaging
phase in the mock image to verify run time dependencies, deployment
tests on product teams local resources, unit tests, functionality
and regression tests of the component in isolation; 
\item Release phase 4: deployment tests on central resources, functionality
tests validating EMI components mutual interaction; 
\item Performance and scalability tests are not mandatory and may occur
both in phase 3 and 4 of EMI release cycle.
\end{enumerate}
After a quality check verification step, formally controlling product
compliance with agreed release policies and guidelines, the release
components candidate for the considered update are deployed on the
inter-component testing infrastructure. All component release candidates
must pass an inter-component testing certification step to enter the
next EMI update. This inter-component testing is performed on EMI
central inter-component testing infrastructure instances.

Integration testing is the part of testing and certification process
of a software product where the product pieces of functionality and
expected behavior are tested against other related grid service components.
In EMI decentralized software development model, testing and certification
are in charge of different Product Teams, each responsible for one
or more software components. Taking place after functional testing
of products in insulation has been successfully carried out, integration
testing then represents the first centralized point of contact among
different products.


\section{Improvements in the EMI 3 release testing scenario}

EMI 1 and EMI 2 releases have shown that many issues were only discovered
during the deployment on the EMI testbed phase and that integration
testing for EMI-2 was not implemented successfully due to delays in
having the testbed in shape since the products were in many cases
not deploy-able. The new strategy conceived for EMI 3 release foresees
a two steps deployment scenario. In the first phase the different
product teams are responsible of providing and maintaining an instance
of their service where the initial testing validation is carried out.
During this phase the SA2 people responsible for the testbed would
take care of hosting and configuring a set of \textquotedbl{}core\textquotedbl{}
services, which lay the foundation of the integration testbed (i.e.
voms, top-bdii) that other services will reference during the interoperability
and integration testing campaigns. Ideally, the resources are monitored
with a nagios instance hosted and maintained by SA2. In a second phase
of the testing process the services are deployed on the central testbed
and a new set of tests is performed centrally as a second independent
check.


\section{Impact of the new testing strategy}

Releasing software with high production level quality, i.e. satisfying
end user expectation in terms of functionality and performance, is
the final goal of every software collaborative project. Software continuous
and effective testing is then a key step in the software development
process to match quality targets. The new testing strategy designed
for EMI 3 release should guarantee a more efficient and complete testing
activity of the software, eventually resulting in increased product
quality and control over the process. The new approach should allow
to shorten the delays experienced in the release process thanks to
early identification of the problems occurring at the interface between
development and testing, thus resulting in a better coupling of the
two activities.
\end{document}
