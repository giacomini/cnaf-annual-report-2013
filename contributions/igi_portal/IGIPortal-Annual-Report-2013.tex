\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}
\title{Accessing Grid and Cloud Services through a Scientific Web portal}

\author{Diego Michelotto,  Marco Bencivenni, Andrea Ceccanti, Daniele Cesini, Enrico Fattibene, 
Giuseppe Misurelli, Elisabetta Ronchieri, Davide Salomoni, Paolo Veronesi, Valerio Venturi,
Maria Cristina Vistoli}

\address{INFN-CNAF}

\ead{diego.michelotto@cnaf.infn.it}

\begin{abstract}
Distributed Computing Infrastructures have dedicated mechanisms to provide user communities 
with computational environments. Concerning the Grid, X.509 certificate is the standard implementation 
of the authentication component. It grants an appropriate level of security; however, users perceive it as
an imposing restriction on the adoption of Grid technology. In addition, the complexity of Grid 
middleware creates demanding practices to support users' requests. In our opinion, the Web interaction 
represents the mainstream to overcome these drawbacks and engage new communities.

In this paper, we present a solution to overcome the aforementioned limitations by providing users with
several Grid and Cloud services (such as job submission, compute provisioning, and data management)
accessible through a community oriented Web portal. Indeed, we have developed the portal within the
Italian Grid Infrastructure framework where the major national user representatives influenced its design,
the implemented solutions and its validation by testing some specific use cases.
\end{abstract}

\section{Introduction}
\label{intro}
In the context of the Italian Grid Infrastructure (IGI)~\cite{IGI}, we collected requirements from several 
communities (such as computational chemistry, astronomy, and earth-science) and investigated 
their particular computing needs. Our aim was to satisfy these users' needs and to hide the X.509 
complexity by using a third-party application to generate, store, and manage personal certificates.
The paper presents the IGI Web portal, based on the Liferay framework designed and developed to 
enable scientists to avail themselves of Grid and Cloud resources.
We reused third-party components such as MyProxy, WS-PGRADE, Distributed Infrastructure with
Remote Control (DIRAC)~\cite{Tsaregorodtsev}, and Grid User Support Environment (gUSE)~\cite{gUSE}. 
The design of the Web portal is general and suitable for different DCIs, and its implementation refers to 
the guidelines provided by the Italian and European Grid Infrastructures (IGI and EGI~\cite{EGI} 
respectively). In this spirit, we also implemented services by using standard modules, called portlets, 
to easily extend the portal to new functionalities. The portal Grid components are already in production, 
whereas the Cloud ones are in a prototypical evolving form. Since May 2013 the portal has supported 
116 users, distributed in 30 VOs, who have submitted 17.8k jobs via DIRAC and 3.4k jobs via 
WS-PGRADE for a total amount of about 380k CPU-hours.

\section{Architecture}
\label{architecture}

We designed the portal architecture with the aim to reuse existing and maintained open solutions for 
all the features mentioned in Section~\ref{intro}. In addition, we adopted Liferay - a popular open 
source portal and collaboration software made of functional units called portlets - to ensure a modular 
Web portal structure. The portlets are components that build dynamic Web contents and use the 
JSR168~\cite{JSR168} and JSR286~\cite{JSR286} standards.

The IGI Web portal architecture, presented in Fig.~\ref{fig:portal_arch}, can be conceptually divided into 
five main layers.

\begin{figure*}[t]
\centering
\includegraphics[width=0.9\textwidth]{Fig1}
\caption{The IGI Web portal architecture: rectangular shapes identify internally developed components,
rounded rectangular shapes show third-party components with local configuration, and finally oval 
shapes represent third-party components used as they are.}
\label{fig:portal_arch}
\end{figure*}

At the highest level,  the Portal AuthN (Authentication) and AuthZ (Authorization) Layer verifies all the 
mandatory credentials provided by the users: X.509 certificate, Virtual Organization (VO) membership 
and Identity Provider (IdP) trusted by the portal.

At the second level, the External AuthZ and AuthN Services Layer supports the upper level in a set of 
operations:
\begin{itemize}
\item Users' credentials vetting leverages the IdPs federations and VOMSes 
(VO Membership Service)~\cite{Cecchini} components invoking single-sign-on 
or \\membership-credential requests.
\item Providing missing credential exploits the portal IdP, online CA, and VOMS components acting as 
fallback and supplying an alternative credential chain.
\item Storing delegated credentials (proxies) makes use of the MyProxy~\cite{Basney} components that 
manage short and long proxies.
\end{itemize}
%
In addition, the CA-bridge component is paramount for the interconnection among the AuthZ portlet 
and the other components involved in this layer, performing all the necessary steps to validate user’s 
identity, to provide a X509 certificate, to generate the Grid credentials and to archive user data. 
The CA-bridge, MyProxy server and online CA components implement a certificate provisioning service 
integrated in the portal framework. The online CA 
supplies MICS (Member Integrated Credential Services) certificates~\cite{MICS} with a 13-months validity.

At the middle level, the Portal Services Layer contains all the portlets implementing data management, 
job submission, application and workflow submissions, and cloud provisioning. While a job is a sequence
of key pairs (attribute, value) based on the JDL, a workflow is a sequence of connected jobs where the 
execution of one or more steps depend on the results of the previous ones. WS-PGRADE consists of 
various portlets (following the Liferay supported standards) providing different functionalities ranging 
from user registration to data management up to workflow handling; however, we only used the 
workflow feature. The Application Specific Management (ASM) portlet builds application-specific 
interfaces and uses the workflows defined in WS-PGRADE. Both WS-PGRADE and ASM interact with the 
Grid and Cloud user support environment (gUSE). The Job and Cloud portlets allow the portal to interact 
with the computing Grid and Cloud resources, while the Data Management  portlet interfaces with the 
storage resources.

At the second to last level, the External Data and Computing Layer provides the tools to handle data 
required by the portlets in the Portal Services Layer. The Data Mover component allows users to transfer 
data among Grid resources, hiding all the complexity of accessing data so that users can spend no effort
to learn Grid data tools. Then, the DIRAC component integrates heterogeneous computing resources 
and provides solutions for submitting jobs to Grid and Cloud infrastructures and for managing data. 
Finally, the gUSE and DCI Bridge components assist the workflow submission.

At the lowest level, the Middleware Resources Layer consists of the Grid and Cloud middleware 
components to provide physical and virtual resources. The interaction with the Grid services is already 
in production, whereas the Cloud one is in a prototype version and still in pre-production.

\section{AuthZ and AuthN Details}
\label{authzauthn}


The two AuthZ and AuthN Layers (described in Section~\ref{architecture} and shown in 
Fig.~\ref{fig:portal_arch}) determine the first access to the portal where users through registration must 
provide the mandatory qualifications to access Grid or Cloud services.

During the authorization phase users must provide the following information: a X.509 certificate issued 
by a trusted EUGridPMA-member CA, the affiliation to a recognized VO and a trusted IdP. If a user only 
has a subset of these credentials, the portal could use them to provide the missing 
ones. Table~\ref{tab:cred} shows the credentials combinations required to successfully conclude the 
registration phase: the \textbf{x} symbol specifies a missing information; the \textbf{o} symbol states an 
available information.

\begin{table}[h]
\centering
\small
\caption{The required credentials for the registration phase.}
\begin{tabular}{c c c c}
\hline
Case  & IDP member & X.509 certificate & VO \\
 \hline
 1 &  \textbf{x}  & \textbf{o} & \textbf{o} \\
 2 &  \textbf{o}  & \textbf{x} & \textbf{x} \\
 3 &  \textbf{o}  & \textbf{o} & \textbf{o} \\
\hline
\end{tabular}
\label{tab:cred}
\end{table}

The request for a personal certificate and its management (such as storing and renewal) often represent 
tedious operations that many users wish to avoid. We addressed this issue: by interfacing the portal with 
an online CA, which provides X.509 certificates to users authenticated by a federated identity 
management system; and by implementing a service to manage these certificates on behalf of the users. 
Depending on their credentials, users can select whether to upload their certificate or ask for a new one 
through the portal.

During the authentication phase, the portal leverages a federated authentication mechanism - based on 
the Security Assertion Markup Language (SAML)~\cite{Hardjono} - to offload the portal from managing 
users' credentials and to exploit a single-sign-on solution. Indeed, the portal trusts all the IdPs belonging
 to the EDUgain federation~\cite{eduGAIN} that interconnects distributed identity federations around the 
 world; therefore all members of these IdPs can access the portal components by using the credentials 
 contained in their own organization IdP. Since Liferay natively supports the Central Authentication 
 Service (CAS)~\cite{CAS} but not SAML, the portal uses the Casshib~\cite{Casshib} software; this enables
 the CAS server to act as a Shibboleth service provider~\cite{Shibboleth}.
 
\section{Data and Computing Services Details}
\label{services}

The Portal and External Data/Computing Services Layers (described in Section~\ref{architecture} and 
shown in Fig.~\ref{fig:portal_arch}) bridge users towards Grid and Cloud resources covering all the 
requirements described in Table~\ref{tab:reqs}. The computing services scenarios comprise the 
submission of: simple Grid jobs with binaries and input data; workflows with complex use cases; 
specific applications oriented to custom interfaces; and Cloud provisioning with IaaS (Infrastructure as a 
Service) allocation. The data service scenario abstracts moving data asynchronously among Grid Storage 
Elements (SEs) in a drag and drop way.

\begin{table}[h]
\centering
\small
\caption{The requirements collected from our user communities.}
\begin{tabular}{p{4cm} p{11cm}}
\hline
Services & Requirements\\
\hline
Authentication& Single-sign-on based authentication\\
and& Personal certificate handling\\
Authorization& Certificate provisioning on demand\\
&\\
Workload& Managing workflows \\
Management& Detailing Job Description Language (JDL) customization\\
& Getting customized Web interface applications\\
&\\
Data& Simplified access to data storage\\
Management& Simplified data moving\\
\hline
\end{tabular}
\label{tab:reqs}
\end{table}

\subsection{Simple Grid jobs}
\label{simplejobs}

Users submit jobs by uploading their executables and input files and by adopting JDL to specify the 
executable parameters. The portal implements this scenario by interfacing the Job portlet with a 
multi-VOs configured DIRAC server. In this context, we developed a portlet that uses the DIRAC 
command line interface, handling the communication between the portal and the DIRAC server. In 
addition, it allows users to: build their JDL by selecting the correct values from a list of attributes and 
settings;  save JDLs as templates for sharing and reusing purposes; show the list of submitted jobs; 
monitor the job state during its execution; retrieve the output; resubmit an ended job; log files in the 
end.

\subsection{Workflows}
\label{workflows}

The workflow submission is a step-by-step procedure for performing complex computations on different
 resources optimizing the overall task. Each step represents a specific portion of the entire calculation - 
 identifying what we call a job. It can follow conditional constraints according to the evolution of the 
 computation. The adopted acyclic workflow structure can assume a simple or complex form in relation 
 to the addressed problem. By combining WS-PGRADE and gUSE the portal allows creating, managing 
 and submitting both types of workflows.
 
 \subsection{Specific applications}
\label{specificapplications}

Some communities may need to submit ad-hoc applications exploiting job and workflow submissions 
according to VO-specific requirements. In this case, we have adopted the following procedure to adapt 
existing applications to the Web portal: first, we analysed the portability of the application on the Grid; 
then, we checked hardware and software requirements (such as RAM, number of CPU cores, and specific 
libraries) as well as required input and output files, and application parameters needed for retrieving log
 and output files at runtime. In addition, we created a script that takes care of getting users' inputs, 
 parameters and files, executing the application and retrieving the output produced during the entire 
 computation. If the application required a job submission, we defined an ad-hoc JDL template, included 
 in the job portlet and available to the users for the submission of the job; otherwise, we created a 
 custom workflow in the WS-PGRADE framework and we implemented a  Web interface, provided by the 
 ASM portlet, which is directly available to the users for the workflow submission. In both cases, we 
 included the mentioned script in the submission operation.
 
 Inspecting produced files and monitoring the application are paramount items for long running 
 applications. The job perusal solution (provided by WMS~\cite{Cecchi}) supports the first item but works
 only with small-size output files to avoid network overload. To overtake these limitations and to 
 integrate application monitoring into the IGI portal, we developed a new mechanism, called application
 progress monitoring, exploiting Grid SEs and Storage Resource Manager (SRM) command line interfaces
 (CLIs) to make temporary and partial output files available to be inspected at runtime. The adopted SRM 
 CLIs mechanism copies selected files from the computing resource where the job is physically running 
 to a Grid SE that stores the files. Then users can directly access these SE files via the job or ASM portlet.

\subsection{Cloud provisioning}
\label{cloud}

Some communities may need to run their applications exploiting the Cloud paradigm. The portal 
implements this scenario by interfacing a Cloud portlet with a set of services that supply resources 
according to the IaaS provisioning model. The services, fed with a pre-built configuration file, can 
interact with various IaaS Cloud providers exploiting the benefits offered by existing Cloud platforms 
such as WNoDeS~\cite{Salomoni}, OpenStack~\cite{OpenStack} and Opennebula~\cite{OpenNebula}. 
The developed portlet exposes a Web interface for each service simplifying users' tasks to create and 
manage new instances.

To instantiate new virtual machines (VMs), users have to upload their own SSH public key~\cite{Barrett}, 
or generate a SSH private and public key pair through the portal. This is necessary to allow logging into 
a VM with root privileges without requiring any password. The keys generated by the portal can be 
retrieved by users in a secure way. Users can then create new VM instances choosing from a list of 
images preloaded in a repository. For each image it is possible to select the size of the cloud 
environment (defined according to the number of cores, memory and disk size) and how many instances 
have to be created at a time; each image has a range size that depends on the Cloud platform it belongs 
to. As soon as users create new instances, a list of their VMs is displayed together with information such 
as architecture, size and status. By selecting the instance name, users are automatically logged into the 
VM with root privileges through a Web terminal~\cite{GateOne} that is part of the portal. The portal lets 
users select VM images offered by different Cloud providers via a repository, which access depends on 
the resource providers internal policies

\subsection{Data management}
\label{data}

In a standard Grid environment users may benefit from a set of command line tools to perform data 
management tasks such as copying files on a Grid SE, registering files in a  LCG File Catalogue 
(LFC)~\cite{Baud}, and replicating files on other Grid SEs. To save learning effort, we designed and 
implemented a data management service~\cite{Bencivenni} sketched in Fig.~\ref{fig:data_arch}. The 
service includes several elements intercommunicating in a secure way.

\begin{figure*}[t]
\centering
\includegraphics[width=0.7\textwidth]{Fig2}
\caption{ The IGI Portal data management service architecture.}
\label{fig:data_arch}
\end{figure*}

The Data Mover component controls and manages every step of the file transfer operations:  uploading 
and downloading files. It controls data transfers through an external storage service - composed of a set
of Storage Resource Manager (StoRM)-based portal SEs~\cite{Magnoni} acting as a cache memory for 
the files - until they are transferred to or downloaded from a Grid SE. The Data Mover is a Web P
ydio-based data management service that implements and extends the functionalities of the Grid data 
management command line tools, exposing a Web interface that manages either Grid files or some 
other types of files. We developed a plug-in for handling data that by the IGI portal allows users to easily
browse the content of the VO file catalogue and to perform operations on either the logical data 
(affecting the catalogue) or the physical files (involving the SE). Table~\ref{tab:ops} details the possible 
operations performed on data.

\begin{table}[h]
\centering
\small
\caption{The possible operations performed on data.}
\begin{tabular}{p{5cm} p{10cm}}
\hline
Data Types & Operations \\
\hline
Logical Data & Creating a new folder;\\
& Deleting an empty folder;\\
& Renaming a folder or file (changing the LFN);\\
& Moving a folder or file (changing the LFN);\\
& Getting detailed information about a file (LFN, GUID - Global Unique Identifier, list of replicas, owner, 
ACL);\\
& Sharing a file with other portal users. \\
&\\
Physical Data & Replicating files on different storage elements;\\
& Downloading files.\\
&\\
Catalogue Data & Deleting files;\\
& Uploading files.\\
\hline
\end{tabular}
\label{tab:ops}
\end{table}

\section*{References}
%\bibliographystyle{spbasic}      % basic style, author-year citations
%\bibliographystyle{spmpsci}      % mathematics and physical sciences
%\bibliographystyle{spphys}       % APS-like style for physics
%\bibliographystyle{plainnat}
\bibliographystyle{unsrt}

\bibliography{portal}   % name your BibTeX data base

\end{document}
