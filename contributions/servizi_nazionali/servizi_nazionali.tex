\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{url}

\begin{document}
\title{National ICT infrastructures and services}

\author{
  S Antonelli$^1$,
  F Bisi$^2$,
  R Giacomelli$^2$,
  S Longo$^1$,
  S Meneghini$^2$,
  M Onofri$^1$,
  R Veraldi$^1$,
  G Vita Finzi$^1$
  and S Zani$^1$
}

\address{$^1$ INFN-CNAF, Bologna, Italy}
\address{$^2$ INFN, Bologna, Italy}

\ead{
  stefano.antonelli@cnaf.infn.it,
  fabio.bisi@bo.infn.it,
  roberto.giacomelli@bo.infn.it,
  stefano.longo@cnaf.infn.it,
  stefano.meneghini@bo.infn.it,
  michele.onofri@cnaf.infn.it,
  riccardo.veraldi@cnaf.infn.it,
  giulia.vitafinzi@cnaf.infn.it,
  stefano.zani@cnaf.infn.it
}

\begin{abstract}

Since the early 90s CNAF has been officially invested with the task to
implement, manage, maintain and coordinate services which are critical
and fundamental due to their importance and catchment area not
only for CNAF itself but also for the whole INFN community. The CNAF
department which carries out this activity is called {\em National ICT
  Infrastructures and Services} and it is composed of three Full Time
persons.
\end{abstract}

\section{Strategic and critical high-priority services}

Some of the services managed by this group are critical for the good
functioning of the whole INFN network and IT infrastructure:

\begin{itemize}

\item DNS for the infn.it TLD: we manage the authoritative
  DNS for the top level domain infn.it and for all the related subnets
  reverse name resolution which points to the local *.infn.it DNS
  servers. This service also acts as a secondary for *.infn.it
  sites. Over 150 zones are managed including non-infn.it zones for
  special purpose activities involving INFN like Grid and other projects.

\item Mail relay MX backup: we manage the mail service backup MX for
  all the @*.infn.it email domains with a 15-day retention policy.

\item Management of the IT infrastructure at the INFN Headquarters: we
  provide support for the INFN headquarters located in Rome,
  either through the use of local hardware or using remote services
  deployed at CNAF. We manage the local network, fundamental
  services like wired and WiFi networks, DNS, mailing, as well as
  implementing specific solutions when necessary to improve and to
  provide new services.

\end{itemize}

\section{Medium-priority non-critical services}

\begin{itemize}

\item National Mailing lists: we manage over 1000 lists for the INFN
  domains with a centralized system based on Sympa.

\item Centralized Web Site management: this is a service which allows
  people to develop a web site for their particular project or
  experiment, in which INFN may be involved. We chose a common
  CMS (Content Management System) based on JOOMLA for everyone. At the moment almost 90 sites
  are managed.

\item NTP service: we manage one of the three Network Time Protocol
  servers for INFN computers.

\item Backup service for the INFN Certification Authority (CA): a daily
  encrypted backup mirrors data of the INFN CA hosted at the Florence
  INFN site.

\item Eduroam and TRIP management: we coordinate the eduroam and TRIP
  802.1x-based WiFi national infrastructure. Actually TRIP is the
  precursor of eduroam inside INFN, uses the same technology and is
  widely adopted by many internal INFN users.

\item Centralized License distribution: we deployed an infrastructure
  for license distribution related to several software packages:
  Ansys, Comsol, Autodesk, NX, Esacomp, Mathematica, Cliosoft,
  Mathlab. It is based on Linux Flex servers. The management of the
  infrastructure is done in collaboration with colleagues from other
  INFN sites: Padova, LNF, Napoli and Pisa.

\item Activation of Microsoft Operating Systems and Office: we manage
  the KMS server for Windows Vista, Windows 7 and Windows 8 Operating
  Systems and Microsoft Office activation for all the INFN computers.

\item National Multimedia Services: the following services are managed:

  \begin{itemize}
  \item Asterisk server for INFN phone conferences
  \item MCU H.323 for INFN video conferences
  \item Real Networks and Flash video servers for streaming INFN events
  \item SeeVogh reflector
  \item vidyo router, part of the CERN vidyo infrastructure
  \end{itemize}

\item Centralized INFN  Authentication and Authorization (AAI): we
  host three servers belonging to the INFN-AAI Service.

\item AFS and Kerberos: we manage three servers for the INFN national
  AFS service.

\item Design, development and maintenance of central infrastructures
  for the Cabibbolab: in particular we manage the electronic agenda
  based on Indico, the document system based on Alfresco and the
  e-mail server for the domain cabibbolab.it

\end{itemize}

\section{Brand New “cutting edge” services}

\begin{itemize}

\item DNS for High-Availability services: this is a new DNS
  architecture recently deployed to meet strict requirements
  concerning the DNS resolution for high-availability services. The
  new DNS architecture is based on a pool of DNS servers distributed
  across INFN sites and implementing a multi-master ISC bind solution
  with underlying Dynamic Loadable Zones and GaleraDB technologies.

\item INFN document service: we have implemented the official INFN
  Document Management System, based on Alfresco. A section (a {\em
    site} in Alfresco parlance) is available for each INFN site, where
  documentation and other material related to local services and
  experiments can be stored. Various scientific collaborations are
  already using this service for their own documents: BELLE-II,
  BESIII, !CHAOS, GINGER, LHCb Italia, PRISMA, ReCaS, ReCaS-PRISMA,
  SPES e SR2S. It is foreseen to extend this service for the INFN
  official collection of documents.

\item Disaster recovery: CNAF coordinates a working group for disaster
  recovery of important services. The involved sites are CNAF and
  LNF. At the moment the project is focused and limited to the INFN
  Information System.

\item Synchronization and desktop backup “Pandora”: the service
  implements a Dropbox-like solution built at CNAF with standard
  open-source tools. It includes a synchronization client,
  multi-platform access via a web interface, data sharing through web
  URLs. It also offers a WebDAV interface and the possibility to
  create mini-sites for the distribution of images of operating
  systems and licensed software that is made available to the entire
  community. The service has been put into production and is available
  to all users belonging to the INFN-wide AAI.

\item INFN video chat service: this is a Skype-like service for INFN
  users implemented on top of the XMPP protocol using open-source
  tools. Any XMMP-compliant client is automatically supported,
  although we suggest Jitsi, which, among other things, provides
  built-in support for confidentiality and non-reputation
  techniques. The service is integrated with the INFN AAI.

\item Collaboration tools aimed at supporting proper software
  engineering practices during software development. These aspects are
  further described in other contributions to this annual report.

\end{itemize}

\section{HW and SW architecture}

The National ICT Infrastructures and Services are mainly implemented
using virtualization technology inside cluster architectures (CentOS,
VMware, OVirt). Some critical, special-purpose services, notably the
DNS, are instead deployed within systems running on a bare hardware
machine. We manage over 160 virtual machines to deliver all the
described services.

\end{document}
