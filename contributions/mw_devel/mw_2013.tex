\documentclass[a4paper]{jpconf}
\usepackage{url}

\begin{document}
\title{Middleware support, maintenance and development}

\author{A.~Ceccanti,
  V.~Venturi,
  D.~Andreotti,
  E.~Vianello}

\address{INFN-CNAF, Bologna, Italy}

\ead{andrea.ceccanti@cnaf.infn.it}

\begin{abstract}

INFN-CNAF plays a major role in the support, maintenance and development
activities of key middleware components (VOMS, StoRM, Argus PAP) widely used in
the WLCG and EGI computing infrastructures. In this report, we discuss the main
activities performed in 2013 by the CNAF middleware development team.

\end{abstract}

\section{Introduction}

The CNAF middleware development team was composed, in 2013, by four persons
dedicated to the support, maintenance and evolution of the following products:

\begin{itemize}

  \item VOMS~\cite{voms}: the attribute authority, administration
    server, APIs and client utilities which form the core of the Grid
    middleware authorization stack;

  \item StoRM~\cite{storm}: the lightweight storage element in
    production at the CNAF Tier1 and in several other WLCG sites;

  \item Argus Policy Administration Point (PAP)~\cite{argus}: the
    Argus administrative interface and policy repository.

\end{itemize}

The main activities for the year centered around support and
maintenance of the software and on the reorganization of the
development and release processes as a consequence of the end of the
EMI project~\cite{emi} in May 2013.

\section{The software development and maintenance process}

In the beginning of 2013 we started organizing our development and maintenance
work following the SCRUM agile process~\cite{scrum}. The objective was to have
a faster release cycle centered around 3-weeks sprints and to track all
activities consistently in order to know where most of the team effort was
spent. We used the INFN internal tracker for the products backlog and to track
other support and maintenance activities~\cite{jira, jira-storm,jira-voms}.
The move to our internal tracker was also motivated by the fact that support
for the former issue tracker used for the middleware (CERN's
Savannah~\cite{cern-savannah}) was being discontinued.

Given that all maintained software was moved, or being moved, to
Github~\cite{github} we decided to adopt the Gitflow branching
model~\cite{gitflow}, in order to have a flexible and understood way of handling
changes and organizing development. At this time we also introduced internal
code reviews leveraging git pull requests and the nice collaboration tools
provided by Github.

As of today, we can say that adopting the agile methodology and modern
development tools boosted our productivity and allowed us to have faster
release cycles that resulted in improved stability of the developed products.

\subsection{Continuous Integration}

During 2013, significant effort has been devoted to the realization of our
continuous integration (CI) and testing system, based on the Jenkins CI
server~\cite{jenkins}. This system was meant to replace the ETICS
system~\cite{etics} that was used for all middleware builds during the EMI
project.

The Jenkins server was configured with build nodes for the main supported
platforms (Scientific Linux 5 and 6, Debian 6), and configurations were created
for all the software packages. The Github Webhooks~\cite{github-hooks}
mechanism provides efficient integration between the code repository and our CI
server, so that whenever a change is pushed to one of the managed repositories
a new build job is started on our CI infrastructure.

Finally, we added automatic deployment test functionality, in order to check
that the latest versions of our products install and run correctly on all
supported platforms. These deployment tests are run nightly. The clean
environment required for the deployment tests is provided by the integration
with a local private cloud infrastructure based on OpenStack~\cite{openstack}.

\section{VOMS}

Work on VOMS focused mainly on:

\begin{itemize}
  \item daily support and maintenance activities;

  \item the full refactoring of the Java APIs based on the newly released EMI
    common authentication library (CANL~\cite{canl}) and the development of the new Java-based
    VOMS command-line clients~\cite{voms-api-java,voms-clients};

  \item the development of a functional and regression testsuite, based on
    robot framework~\cite{voms-testsuite};

  \item the VOMS Admin transition to a standalone process running on an
    embedded Jetty container \cite{jetty-embedded}.

\end{itemize}

\section{StoRM}

In march 2013, an important refactoring activity was launched on the StoRM
codebase to align it with the tools and processes already in place for
other products (VOMS and Argus PAP) maintained by the group. This meant moving the
codebase to Github, adopting modern build tools (maven~\cite{maven}) for the
backend server, rationalizing the packaging and including StoRM in the CI
infrastructure.

During this transition, two test suites were developed to assess the service
stability and scalability:

\begin{itemize}
  \item a functional and regression testsuite, based on robot framework~\cite{robot,storm-testsuite}
  \item a load testsuite, based on the Grinder tool~\cite{grinder,load-testsuite}
\end{itemize}

In the final months of the year, the team focused on improving the performance
of the StoRM GridHTTPS server in view of the ATLAS DQ2 to RUCIO file renaming
campaign~\cite{atlas-renaming}, and fixing a set of high priority
vulnerabilities~\cite{storm-vuln} reported by the EGI Software Vulnerability Team.

\section{Argus}

In 2013, work on the Argus PAP focused on daily support and maintenance activities
and the preparation of the EMI-3 release, for which the main change was the adoption of
the EMI CANL library~\cite{canl}.

\section{Web presence}

The VOMS and StoRM web sites~\cite{voms,storm} have been migrated to Github
pages~\cite{github-pages}, a free hosting service provided by Github.
There are several advantages in using Github pages for the web
presence of our products:

\begin{itemize}

  \item no hosting costs;

  \item versioned content and automatic publishing: all content is hosted on a
    Github repository, whenever a change is pushed to the repository the new
    content is published on the web site;

  \item effective workflow: the same workflow used for reviewing changes in the
    code can be used for the website (i.e., forked preview
    repositories, pull requests);

  \item simple but powerful static content-management engine based on
    Jekyll~\cite{jekyll}, which allows for easy management and update of the
    content of the web site by editing markdown pages~\cite{markdown}.

\end{itemize}

\section{Support}

The Global Grid User Support (GGUS) system is the main support access point for
EGI and WLCG. In the EGI support model, requests are first routed to the
Deployed Middleware Support Unit (DMSU), managed by EGI, which provides expert
support by Grid service operators. If an incident cannot be resolved by the
DMSU, it is escalated to specialized, third-level support, typically provided
by middleware developers.

In order to have a clearer view on support requests assigned to our support units
(VOMS, StoRM and Argus), we developed a simple web tool, termed ggus-monitor,
that groups support requests per priority and status, so that it is always evident
on which requests the support team should focus at any given time.

\section{Future work}

Besides ordinary support and maintenance, in the future we will focus on
the following activities:

\begin{itemize}

  \item Refactoring of the StoRM services, to reduce code-base size and maintenance
    costs, to provide horizontal scalability to all services and to decouple
    the WebDAV service from the SRM backend service;

  \item Evolution of the VOMS attribute authority for better integration with
    SAML federations;

  \item Continuous integration and delivery, by leveraging lightweight
    virtualization environments (e.g., Docker) for integration testing and
    simplified deployment in production.

\end{itemize}

\section*{Acknowledgements}

This work is dedicated to the memory of our friend and guide Valerio Venturi,
suddenly deceased on December, 25\textsuperscript{th} 2013. His kind presence,
phenomenal sense of humour and sharp technical vision are now missing
ingredients in our daily working life.

\section*{References}
\begin{thebibliography}{99}

  \bibitem{egi} European grid Infrastructure \url{http://www.egi.eu}
  \bibitem{wlcg} The Worldwide LHC computing Grid \url{http://wlcg.web.cern.ch}
  \bibitem{voms} The VOMS website \url{http://italiangrid.github.io/voms}
  \bibitem{voms-clients} VOMS clients code repository \url{https://github.com/italiangrid/voms-clients}
  \bibitem{voms-api-java} The VOMS Java APIs \url{https://github.com/italiangrid/voms-api-java}
  \bibitem{argus} Argus authorization service website \url{http://argus-authz.github.io}
  \bibitem{storm} StoRM website \url{http://italiangrid.github.io/storm}
  \bibitem{jenkins} Jenkins \url{https://jenkins-ci.org/}
  \bibitem{github} GitHub \url{https://github.com/}
  \bibitem{openstack} Openstack \url{http://www.openstack.org}
  \bibitem{scrum} SCRUM \url{http://bit.ly/scrum-wikipedia}
  \bibitem{jira} INFN issue tracker \url{https://issues.infn.it}
  \bibitem{jira-storm} StoRM on INFN JIRA \url{https://issues.infn.it/jira/browse/STOR}
  \bibitem{jira-voms} VOMS on INFN JIRA \url{https://issues.infn.it/jira/browse/VOMS}
  \bibitem{maven} Apache Maven \url{http://maven.apache.org}
  \bibitem{robot} Robot framework \url{http://robotframework.org/}
  \bibitem{grinder} The Grinder \url{http://grinder.sourceforge.net}
  \bibitem{emi} The European Middleware Initiative \url{http://www.eu-emi.eu}
  \bibitem{gitflow} The Git flow branching model \url{http://nvie.com/posts/a-successful-git-branching-model/}
  \bibitem{canl} The EMI Common Authentication Library \url{https://github.com/eu-emi/canl-java}
  \bibitem{voms-testsuite} The VOMS clients testsuite \url{https://github.com/italiangrid/voms-testsuite}
  \bibitem{storm-testsuite} The StoRM testsuite \url{https://github.com/italiangrid/storm-testsuite}
  \bibitem{load-testsuite} The StoRM load testsuite \url{https://github.com/italiangrid/grinder-load-testsuite}
  \bibitem{etics} The ETICS system \url{http://etics-archive.web.cern.ch/etics-archive/}
  \bibitem{github-hooks} Github webhooks \url{https://help.github.com/articles/about-webhooks}
  \bibitem{github-pages} Github pages \url{https://pages.github.com}
  \bibitem{cern-savannah} CERN's Savannah issue tracker \url{https://savannah.cern.ch}
  \bibitem{storm-vuln} StoRM Vulnerability Advisory \url{http://j.mp/storm_vuln}
  \bibitem{atlas-renaming} Serfon, C., et al. ATLAS DQ2 to Rucio renaming infrastructure. 2014 J.~Phys.: Conf. Ser. 513 042008
  \bibitem{jekyll} The Jekyll static site generator \url{http://jekyllrb.com}
  \bibitem{markdown} Markdown \url{https://en.wikipedia.org/wiki/Markdown}
  \bibitem{jetty-embedded} Embedding Jetty \url{https://wiki.eclipse.org/Jetty/Tutorial/Embedding_Jetty}

\end{thebibliography}
\end{document}
